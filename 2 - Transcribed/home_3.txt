Teaching
3

Interacting
2

30979.830236 User said: move forward
30979.832530 Got reaction from knowledge processor "obey"
31087.512294 User said: move forward
31087.514190 Got reaction from knowledge processor "obey"
31117.308157 User said: to lean is to move back and move back and move back
31117.310764 No reaction from knowledge processor "obey"
31117.310943 No reaction from knowledge processor "introspect action"
31117.311078 No reaction from knowledge processor "learn action"
31117.311190 Got reaction from knowledge processor "not understood"
31117.342728 Robot said: Sorry, I don't understand.
31141.437406 User said: to answer is to raise your left arm and say hello
31141.438794 No reaction from knowledge processor "obey"
31141.438851 No reaction from knowledge processor "introspect action"
31141.439210 Got reaction from knowledge processor "learn action"
31141.487885 Robot said: Yes, to answer is   to raise my left arm and to say hello.
31182.155175 User said: to dance is to turn left turn right turn left
31182.157688 No reaction from knowledge processor "obey"
31182.157749 No reaction from knowledge processor "introspect action"
31182.157885 Got reaction from knowledge processor "learn action"
31182.193573 Robot said: Yes, to dance is to turn left.
31206.173359 User said: to watch is to look forward
31206.175342 No reaction from knowledge processor "obey"
31206.175406 No reaction from knowledge processor "introspect action"
31206.175960 Got reaction from knowledge processor "learn action"
31206.203439 Robot said: Yes, to watch is to look forward.
31243.059239 User said: to be happy is to raise your arms up and turn left
31243.061282 No reaction from knowledge processor "obey"
31243.061341 No reaction from knowledge processor "introspect action"
31243.061400 No reaction from knowledge processor "learn action"
31243.061446 Got reaction from knowledge processor "not understood"
31243.093867 Robot said: Sorry, I don't understand.
31275.836618 User said: to run is to move forward
31275.838608 No reaction from knowledge processor "obey"
31275.838671 No reaction from knowledge processor "introspect action"
31275.838816 Got reaction from knowledge processor "learn action"
31275.867780 Robot said: Yes, to run is to move forward.
31317.428584 User said: to eat is to raise your right arm and lower your right arm
31317.430771 No reaction from knowledge processor "obey"
31317.430826 No reaction from knowledge processor "introspect action"
31317.431542 Got reaction from knowledge processor "learn action"
31317.481792 Robot said: Yes, to eat is   to raise my right arm and to lower my right arm.
31343.769582 User said: to welcome is to move back and look down
31343.771614 No reaction from knowledge processor "obey"
31343.771675 No reaction from knowledge processor "introspect action"
31343.771903 Got reaction from knowledge processor "learn action"
31343.824528 Robot said: Yes,   to welcome is to move back and to look down.
31404.214811 User said: to dream is to look up
31404.216402 No reaction from knowledge processor "obey"
31404.216463 No reaction from knowledge processor "introspect action"
31404.216562 No reaction from knowledge processor "learn action"
31404.216608 Got reaction from knowledge processor "not understood"
31404.244964 Robot said: Sorry, I don't understand.
31484.806556 User said: to bump is to move right and say aie
31484.808428 No reaction from knowledge processor "obey"
31484.808485 No reaction from knowledge processor "introspect action"
31484.808576 No reaction from knowledge processor "learn action"
31484.808622 Got reaction from knowledge processor "not understood"
31484.835645 Robot said: Sorry, I don't understand.
31500.281070 User said: to bump is to turn right move forward and say aie
31500.283621 No reaction from knowledge processor "obey"
31500.283682 No reaction from knowledge processor "introspect action"
31500.283742 No reaction from knowledge processor "learn action"
31500.283794 Got reaction from knowledge processor "not understood"
31500.323718 Robot said: Sorry, I don't understand.
31521.735787 User said: say aie
31521.740824 Got reaction from knowledge processor "obey"
31521.764614 Robot said: Aie
31534.114985 User said: turn right
31534.116726 Got reaction from knowledge processor "obey"
31555.285738 User said: to bump is to turn right and move forward and say aie
31555.287915 No reaction from knowledge processor "obey"
31555.287976 No reaction from knowledge processor "introspect action"
31555.288380 No reaction from knowledge processor "learn action"
31555.288448 Got reaction from knowledge processor "not understood"
31555.322986 Robot said: Sorry, I don't understand.
31595.560405 User said: to bump is to turn right and go forward
31595.563089 No reaction from knowledge processor "obey"
31595.563150 No reaction from knowledge processor "introspect action"
31595.563316 No reaction from knowledge processor "learn action"
31595.563362 Got reaction from knowledge processor "not understood"
31595.604465 Robot said: Sorry, I don't understand.
31606.037229 User said: to bump is to turn right, move forward and say aie
31606.039091 No reaction from knowledge processor "obey"
31606.039151 No reaction from knowledge processor "introspect action"
31606.039441 Got reaction from knowledge processor "learn action"
31606.092227 Robot said: Yes, to bump is to turn   right, to move forward and to say aie.
31654.035516 User said: what is to answer
31654.036879 No reaction from knowledge processor "obey"
31654.036998 Got reaction from knowledge processor "introspect action"
31654.072048 Robot said: To answer is   to raise my left arm and to say hello.
31662.374626 User said: answer
31662.376652 Got reaction from knowledge processor "obey"
31662.428512 Robot said: hello
31683.000439 User said: what is to dance
31683.001712 No reaction from knowledge processor "obey"
31683.001800 Got reaction from knowledge processor "introspect action"
31683.014757 Robot said: To dance is to turn left.
31692.211671 User said: what is to watch
31692.212772 No reaction from knowledge processor "obey"
31692.212850 Got reaction from knowledge processor "introspect action"
31692.227801 Robot said: To watch is to look forward.
31707.455779 User said: what is to run
31707.457733 No reaction from knowledge processor "obey"
31707.457821 Got reaction from knowledge processor "introspect action"
31707.477558 Robot said: To run is to move forward.
31715.443308 User said: what is to eat
31715.444443 No reaction from knowledge processor "obey"
31715.444557 Got reaction from knowledge processor "introspect action"
31715.478400 Robot said: To eat is   to raise my right arm and to lower my right arm.
31720.330238 User said: what is to dream
31720.331463 No reaction from knowledge processor "obey"
31720.331594 No reaction from knowledge processor "introspect action"
31720.331717 No reaction from knowledge processor "learn action"
31720.331767 Got reaction from knowledge processor "not understood"
31720.356125 Robot said: Sorry, I don't understand.
31725.155830 User said: what is to dream
31725.157173 No reaction from knowledge processor "obey"
31725.157253 No reaction from knowledge processor "introspect action"
31725.157292 No reaction from knowledge processor "learn action"
31725.157336 Got reaction from knowledge processor "not understood"
31725.190112 Robot said: Sorry, I don't understand.


