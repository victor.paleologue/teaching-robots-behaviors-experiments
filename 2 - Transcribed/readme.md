Protocol
--------

A participant joins the experimenter and a Pepper robot in a small meeting room (Vanqor, a room for 4 persons at SBRE).

The experimenter explains the experiment and recalls that no personal information will be saved.

A sheet of instructions is lent to the participant. It displays speech formulas to trigger actions already known by the robot, such as "Say *", "Move \<forward, backward\>", etc... It also summarizes how to teach behaviors: "to \<verb\> is to \<verbal group\>{, ...}", using existing verbs and in one sentence, and gives two examples. Sheets are themed according to the domains of "home", "care" and "business": examples are taken from these domains, and the color of the titles are different.

The participant is given 10 minutes to teach as many behaviors as possible to the robot, using natural language. When the participant says something to the robot, it is transcribed (as quickly as possible, by the experimenter) to the robot, which responds autonomously.

The sheets can be found under the file names "known actions sheet \<theme\>.svg".

After the 10 minutes, the participant is asked to rate:
- between 1 to 5, how successful the teaching felt.
- between 1 to 5, how successful the overall interaction felt.

The participant then leaves the room and the experimenter asks the robot to recall the taught behaviors, using natural language matching the introspection algorithm.

Participants
------------

9 voluntary employees of SBRE were chosen from the software teams. They knew how to interact with Pepper robots, but never did the experiment before.

Primary Data
------------

The data collected during the experiment can be found in this directory and match the naming template "\<theme\>_\<session_number_for_theme\>.txt.

Data files start with the respective ratings for how the teaching and the overall interaction felt.

The rest of the file is the copy of the logs produced by the program from the beginning of the teaching to the end of the recall of taught behaviors, and includes indications of:
- what was heard by (or here, transcribed to) the robot.
- which reasoning algorithm was tried, and which one produced a resonse.
- what the robot said back.
- the time at which each of these events occurred, in seconds.

The first sentence said to the robot marks the beginning of the teaching. The sentence before the first introspection marks the end of the teaching.

Analysis
--------

All the data was merged together into `logs_and_measurements.ods`.
Each session is in a separate sheet,
named as `<theme_initial><session_for_theme>`,
where theme initial
is the first letter of the theme name.
This name serves as a short session identifier.

Transcriptions were annotated manually,
by the experimenter, to tell:
- why instructions may have been misunderstood.
  `A` stands for ASR errors (here, none),
  `S` stands for semantic extraction error,
  `U` stands for unsupported by the behavior.
- when a new behavior is being taught
  (column `NB`)
- when a behaviors has been successfully taught
  (column `BS`)

The statistics published in RO-MAN 2017
were computed in that file.

For the thesis publication,
we exported the annotated data as
`transcription_<session_id>_annotated.tsv`,
to process them using Python scripts.
These transcriptions do not include
the instructions used to recall
the taught behaviors.

Note that preliminary results
are omitted from published statistics.
