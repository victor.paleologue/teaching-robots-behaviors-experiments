#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy
import pandas
import scipy.stats
import os

def test_independence(contingency_table):
    if (contingency_table < 5).sum(1).sum(0) > 0:
        print('There is fewer than 5 occurrences of a sample, test may be invalid')
    chi2, p, dof, expected = scipy.stats.chi2_contingency(contingency_table)
    print('chi2 = %s\np = %s' % (chi2, p))

    if p < 0.05:
        print('Variables are not independent')
    elif p > 0.95:
        print('Variables are independent')
    else:
        print('Inconclusive result')


sessions_info = dict({
    'transcription_b1_annotated.tsv': ('b1', 'Business'),
    'transcription_b2_annotated.tsv': ('b2', 'Business'),
    'transcription_b3_annotated.tsv': ('b3', 'Business'),
    'transcription_c1_annotated.tsv': ('c1', 'Care'),
    'transcription_c2_annotated.tsv': ('c2', 'Care'),
    'transcription_c3_annotated.tsv': ('c3', 'Care'),
    'transcription_h1_annotated.tsv': ('h1', 'Home'),
    'transcription_h2_annotated.tsv': ('h2', 'Home'),
    'transcription_h3_annotated.tsv': ('h3', 'Home'),
})

# Data regroups all the data, per gathered utterance.
data = None
for file_name, session_info in sessions_info.items():
    dir_path = os.path.dirname(__file__)
    file_path = os.path.join(dir_path, file_name)
    session_data = pandas.read_csv(file_path, sep='\t')
    session_data['Session'], session_data['Theme'] = session_info
    if data is None:
        data = session_data
    else:
        data = data.append(session_data)

# Distinguish user instructions
user_instructions = data[(data.Speaker == 'User')]

# For the counts to work properly,
# we replace NaNs from Mis columns with an arbitrary symbol.
# 'G' stands for good.
user_instructions.Mis.fillna('G', inplace=True)

# Data by session regroups statistics for each session.
data_by_session = data.groupby(['Session', 'Theme'])
data_sum_by_session = data_by_session.sum()
data_count_by_session = data_by_session.count()
# Puts "Theme" as a column rather than an index
data_sum_by_session.reset_index(level=1, inplace=True)

# Testing that the NLU is theme-independent.
#-------------------------------------------
print('--------------------------------------')
mis_theme_count = pandas.crosstab(
    user_instructions.Theme,
    user_instructions.Mis,
    margins=True)
print('Understanding vs. Theme, contingency table:\n%s'
        % mis_theme_count)
test_independence(mis_theme_count)

# Testing that the number of successfully taught behaviors
# is independent from the theme.
#-------------------------------------------
print('--------------------------------------')
bs_theme_count = pandas.crosstab(
    data_sum_by_session.Theme,
    data_sum_by_session.BS,
    margins=True)
print('Understanding vs. Taught Behaviors, contingency table:\n%s'
        % bs_theme_count)
test_independence(bs_theme_count)