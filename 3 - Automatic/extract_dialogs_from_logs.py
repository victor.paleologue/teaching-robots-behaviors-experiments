#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(
    description='Extract dialogs from a log file, and output them as tab-separated values')
parser.add_argument(
    'log_files', type=str, nargs='+',
    help='Paths to log files to extract dialogs from')
args = parser.parse_args()

for log_path in args.log_files:
    with open(log_path, 'r') as log_file:
        print('%s:' % log_path)
        for line in log_file.readlines():
            # Valid dialog logs are as follows:
            # <timestamp> <speakerIndicator>: <utterance>
            # <speakerIndicator> can be H (for human) or R (for robot)
            timestamp, _, log = line.partition(' ')
            speakerIndicator, _, utterance = log.partition(': ')

            speaker = str()
            if speakerIndicator is 'H':
                speaker = "User"
            elif speakerIndicator is 'R':
                speaker = "Robot"
            else:
                continue

            print('%s\t%s\t%s'
                  % (timestamp, speaker, utterance.strip()))
        print('')
