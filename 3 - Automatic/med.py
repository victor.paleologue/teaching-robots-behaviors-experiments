#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import editdistance

parser = argparse.ArgumentParser(
    description='''Evaluates the average Minimum Edit Distance (MED) between two groups of strings,
as a ratio of the length of original strings.
Groups are identified by their label.''')
parser.add_argument(
    'file', type=str,
    help='The path to a file providing tab-separated values:\nLABEL\tEXPECTED\tACTUAL')
args = parser.parse_args()

meds = list()
meds_by_label = dict()
meds_by_theme = dict()
with open(args.file) as tsv_file:
    lineCount = 0
    for entry in tsv_file.readlines():
        lineCount += 1
        if len(entry) <= 1:
            continue # skip empty lines

        entry = entry.split('\t')
        if (len(entry) < 3):
            raise Exception('Not enough fields at line %s' % lineCount)

        label = entry[0].strip()
        theme = label[0]
        expected = entry[1].strip()
        actual = entry[2].strip()

        distance = editdistance.eval(expected, actual)
        med = float(distance) / float(len(expected))
        meds.append(med)

        if label not in meds_by_label.keys():
            meds_by_label[label] = list()
        meds_by_label[label].append(med)

        if theme not in meds_by_theme.keys():
            meds_by_theme[theme] = list()
        meds_by_theme[theme].append(med)

print("MED avg. per session:")
for key, value in meds_by_label.items():
    average = sum(value) / float(len(value))
    print("- %s: %s" % (key, average))

print("MED avg. per theme:")
for key, value in meds_by_theme.items():
    average = sum(value) / float(len(value))
    print("- %s: %s" % (key, average))

med_average = sum(meds) / float(len(meds))
print("Overall MED average: %s" % med_average)

