Experiment Notes for Teaching Behavior Compositions using ASR
=============================================================

Below, notes about events occurred during the experiments.
Each participant is a different person from SBRE, even the
"bis" ones.

Participant #1
--------------

2018-08-09T11:34

The participant has a clear French accent, causing difficulties for speech
recognition.

At 11:35, I had to close the charging hatch, I forgot it.

When ASR fails on a verb, the participant tries another verb.

"What do I have to raise?" sneaked in from the Semantic Engine.

"Search" -> why has it failed?

The participant says "to raise" as if it was a valid action. Was the sheet
unclear about it?

"Help" was clearly recognized but could not be done. Was the feedback not
clear enough?

A recursive behavior composition may happen by mistake, because of the
step-by-step approach. Maybe we need a different feedback from the "not
understood one" here.

I gave advice on how to pronounce "forward" and "follow".

At some point, the participant omits the "to" in front of verbs.

We did not manage to ask the robot "What is to search" using the voice.
Only later I managed to input that programmatically, and have the
robot produce a response for this, hence producing the data required
for the experiment.

Since this piece of introspection is not the subject of the experiment,
I will use that every time the issue occurs.

Participant #2
--------------

2018-08-09T16:06

"No" has been utterred as expected at design time.

"To say" is often followed by a pause.

"To greet" is hard to go through.

"A(To search) B(is to raise the left arm) C(and to look forward)".
B has not been understood by the ASR, but the participant did not
notice and told C.

The ASR seems to have hard times with pieces of sentences
(instead of full sentences).

The program crashed during the experiment. Experiment aborted
for participant #2.

Participant #2 bis
------------------

2018-08-10T10:49

Perfect teaching at first shot, but ASR got a slightly different
result, despite his or her good accent.

The feedback says what was understood, but the participant did
not get it.

The participant did not know whether the robot may start the
behavior once taught.

Directional microphones are annoying, the participant has to
move around with the robot turning around.

"is to <...>" considered part of the elements of the teaching.

"Is that all?" after the robot responds for something else
(semantic engine!). Teaching should avoid more often keeping on
if the conversation shifts to another subject.

The following pattern really is too misleading.
H: Say
R: What do I have to say?
H: ...
It is still that bug with the semantic engine.

Obeying & teaching should be more tied, because direct orders
may be part of the teaching itself.

Participant #3
--------------

2018-08-09T11:01

First participant to start with an order.

H: To greet
R: Ok
H: To greet is to
R: `<not_understood>` -> should have been understandable?

R: Is that all?
H: No -> felt ok, but wrong teaching!

Laughing with them to errors make them more confident and
more annoyed by the lack of understanding of the robot.

Full teaching in the middle of another teaching is messed up.
After some time, the participant started to play the teaching
behavior correctly.

Participant #3 bis
------------------

2018-08-09T14:39

Say + ... (again!)

When doing the examples, people realize the limitations of
the ASR.

"is to" alone.

Can you? (first time in the experiment).
Robot always responds "ok" (bad).

Ok is misleading.

"What can you do?"

"To `<infinitive>`, `<imperative> {`, `...}`"

"How do you `<present>`?"

"Come forward" (rather than move forward)

"Can I teach you something new?"

(More time spent testing the limit of the dialog engine).

Does not realize that the robot would repeat the whole teaching
in the end, because he or she did not see a working behavior
before.

R: "Is that all?"
H: "no" -> every time!

H: "Say yes" in the middle of the teaching:
R: "yes"
R: "ok" < should have been filtered out.

Highlights the lack of the diversity of supported interactions.

Participant congratulates the robot on successful teaching.
The robot never accept congratulations.

Participant #4
--------------

2018-08-09T14:57

Realizes that objects aren't dealt with.

"Can you say hello?", again.

Goes around the robot to evaluate the tracking.

Talks only when the sentence is clear in his or her mind.

Until now, everyone restarted at some point the teaching,
because of misunderstanding in a teaching.

"No" is used to restart, but it does not really, so it leads
to twisted compositions.

"Please forget about `<substantive>`".

"`<imperative>` please".

H: Is to move...
R: Is that all? -> like for the "Say", annoying!

"is to" --ASR--> "east to"

Does not speak so loud, so only few words are spotted
from time to time.

Reading the recall of the teaching is really useful.

Participant #5
--------------

2018-08-09T15:14

Direct teaching, great.

The participant have a better English and talks louder.

Tries to say it all in one shot.
Restarts all the time.
No is again used to restart, not to go back one step.

"Forget it" should be in the sheet, definitely...

H: "To not look at me"...
R: "Ok, I'm not looking at you" (semantic engine)
H (off): "Ok, it makes sense to me!"

Works quite better by doing that one-shot version.
ASR does not work so well on huge sentence.

Turn taking is the issue for this participant.

"Ok" is very ambiguous.

When a verb was difficult to recognize by the ASR, the participant
re-did the teaching with a different verb. It is going to be
considered as a separate teaching, so that we can take into account
the failure with the initial verb.

Participant #6
--------------

2018-08-09T15:46

The robot hardly understands with the ASR.

Surprised the robot does not execute the behavior once taught.

Accent has a great impact on the teaching quality.

Mostly full sentences again.

Forgot the list of known behaviors, and had to cut a teaching.

I think my presence disturbed the experiment.

Participant #7
--------------

2018-08-09T16:15

Did not understand: the robot did not even hear him or her.

Accent is problematic too.

"to say..." -> always there...

"Je suis où du coup ?".

"Ok" to close the teaching.

Participant #8
--------------

2018-08-09T16:57

Not sure that it is possible to make put more than two verbs
per level of composition.

"Cancel". (too bad)

Sorry, I don't understand -> restart from the beginning again.

Does recursive teaching "to say XX is to ... and say hello".

After a weird erroneous teaching, the participant says he or she
has understood. (some extra instructions may be enough to rule
out this issue)

Did not really feel there was a good feedback.

The head nod was misleading and made him or her think that it was
understood, but in fact it was not.

He or she would have liked to know how to cancel, come back
in the teaching.

In the sheet, it is said "in one sentence", but it is understood
as "in one utterance".

The participant expressed his or her need for complements in the
teaching.

"Is that all?" was not understood the first time. The "No" in this
case is also unclear in terms of effect.

Suggest a "while" to do things in parallel.

Participant #9
--------------

2018-08-09T17:40

Head nod disturbed that from the beginning.
Even though he or she was talking too low, the head nods
occur!

Bad accent make it difficult. I had to give advice on how to pronounce.

ASR server went down for too long, experiment aborted.

Participant #9 bis
------------------

2018-08-21T09:57

Good English speaker.

First teachings with "to greet" failed.

H: "To welcome..."
R: "Ok"
H: "To welcome..."
R: "Ok" -> when in fact the teaching is going bad!

The robot cuts the turn of the participant more often than usual.
The participant usually succeeds better by saying it all in one shot,
but the robot cuts his or her speech very often. The participant
realizes and complains about this.

The experiment was shortly interrupted because of an autonomous activity
popping up. The taught data in the meantime has been forgotten, so it
was retaught with the exact same text for the final introspection.

H: "You stubborn" -> no response.

When the robot turns around autonomously.

H: "Can you promote?" -> no response.

H: "Yeeeesss" -> not understood.

The participant did not try to check what the robot does for the primitive
actions.

The participant did not realize the teaching could be done step-by-step.
He or she has had difficulties to find vocabulary. He or she also admitted
to try to trick the robot.
