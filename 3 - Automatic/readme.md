Protocol
--------

A participant joins the experimenter and a Pepper robot in a small meeting room
(Vanqor, a room for 4 persons at SBRE).

The experimenter explains the experiment as follows, and recalls that
no personal information will be saved.

A sheet of instructions is lent to the participant. It displays speech formulas
to trigger actions already known by the robot, such as "Say \*",
"Move \<forward, backward\>", etc... It also summarizes how to teach behaviors:
"to \<verb\> is to \<verbal group\>{, ...}", using existing verbs and
in one sentence, and gives two examples. Sheets are themed according to the
domains of "home", "care" and "business": examples are taken from these
domains, and the color of the titles are different.

The participant is given 10 minutes to teach as many behaviors as possible to
the robot, using natural language. The participant must talk when the robot
shows it is listening (blue LEDs on the shoulders are turned on). When the
participant says something to the robot, it is automatically transcribed
(using a speech-to-text engine) to the robot, which responds autonomously.

The sheets can be found under the file names "known actions sheet \<theme\>.svg",
and are exactly the same as the ones used for the experiment Behavior Composition #2.

After the 10 minutes, the participant is asked to rate:
- between 1 to 5, how successful the teaching felt.
- between 1 to 5, how successful the overall interaction felt.

The participant then leaves the room and the experimenter asks the robot to
recall the taught behaviors, using natural language matching the introspection algorithm.


Participants
------------

9 voluntary employees of SBRE were chosen from the software teams.
They knew how to interact with Pepper robots, but never did the experiment before.
Their level of English was noticeably variable.

The 3 themes have been covered by 3 participants each.
They were attributed randomly among the themes left to
be covered (last participant got his or her theme
imposed).

How to read these results
-------------------------

The result files are in this directory and match the naming template `<transcription>_<session_number>.txt`.

Each file starts with:
- the theme
- the rating for the teaching success
- the rating for the interaction success.

The rest of the file is the copy of the logs produced by the program from the beginning of the teaching to the end of the recall of taught behaviors, and includes indications of:

- what was recognized automatically by the robot from the speech.

- which reasoning algorithm was reacted, how their response suggestion changed.

- what the robot said back.

- the time at which each of these events occurred, in seconds relative to
  an arbitrary point in time.

The first sentence said to the robot marks the beginning of the teaching.
The sentence before the first introspection marks the end of the teaching.
The first introspection can be recognized because it is the first of a series
of "what is to \<verb\>" questions.

Analysis
--------

Applying `extract_dialogs_from_logs.py` on the transcription logs produces
`extracted_dialogs.txt`. These dialogs were then entered in
`logs_and_measurements.ods` to be processed.

This file regroups all the dialog entries, and annotate them with indicators,
such as the cause of the misrecognition, whether a new behavior is being
taught, whether it succeeded, etc... All details about the indicators are
to be found in later publication.

Each session is in a separate sheet,
named as
`<theme_initial><session_number_for_theme>p<session_number>`,
where theme initial
is the first letter of the theme name.
This name serves as a short session identifier.

Transcriptions were annotated manually,
by the experimenter, to tell:
- why instructions may have been misunderstood.
  `A` stands for ASR errors,
  `S` stands for semantic extraction error,
  `U` stands for unsupported by the behavior.
- when a new behavior is being taught
  (column `NB`)
- when a behaviors has been successfully taught
  (column `BS`)

The MED indicator was calculated separately, using a summary of all behavior
taught vs. what was expected, in `expected_vs_taught.tsv`, and the script
`med.py`.

The statistics published in ICSR 2018
were computed in that file.

For the thesis publication,
we exported the annotated data as
`transcription_<session_id>_annotated.tsv`,
to process them using Python scripts.

We also imported the annotated data
from previous experiment,
"Behavior Composition #2 (Transcribed)",
in order to perform comparative analysis.