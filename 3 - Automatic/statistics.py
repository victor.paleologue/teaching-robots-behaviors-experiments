#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy
import pandas
import scipy.stats
import os

def test_independence(contingency_table):
    if (contingency_table < 5).sum(1).sum(0) > 0:
        print('There is fewer than 5 occurrences of a sample, test may be invalid')
    chi2, p, dof, expected = scipy.stats.chi2_contingency(contingency_table)
    print('chi2 = %s\np = %s' % (chi2, p))

    if p < 0.05:
        print('Variables are not independent')
    elif p > 0.95:
        print('Variables are independent')
    else:
        print('Inconclusive result')


sessions_info = dict({
    'transcription_b1p4_annotated.tsv': ('xp3', 'b1p4', 'Business'),
    'transcription_b2p8_annotated.tsv': ('xp3', 'b2p8', 'Business'),
    'transcription_b3p9_annotated.tsv': ('xp3', 'b3p9', 'Business'),
    'transcription_c1p1_annotated.tsv': ('xp3', 'c1p1', 'Care'),
    'transcription_c2p5_annotated.tsv': ('xp3', 'c2p5', 'Care'),
    'transcription_c3p7_annotated.tsv': ('xp3', 'c3p7', 'Care'),
    'transcription_h1p2_annotated.tsv': ('xp3', 'h1p2', 'Home'),
    'transcription_h2p3_annotated.tsv': ('xp3', 'h2p3', 'Home'),
    'transcription_h3p6_annotated.tsv': ('xp3', 'h3p6', 'Home'),
    'transcription_b1_annotated.tsv': ('xp2', 'b1', 'Business'),
    'transcription_b2_annotated.tsv': ('xp2', 'b2', 'Business'),
    'transcription_b3_annotated.tsv': ('xp2', 'b3', 'Business'),
    'transcription_c1_annotated.tsv': ('xp2', 'c1', 'Care'),
    'transcription_c2_annotated.tsv': ('xp2', 'c2', 'Care'),
    'transcription_c3_annotated.tsv': ('xp2', 'c3', 'Care'),
    'transcription_h1_annotated.tsv': ('xp2', 'h1', 'Home'),
    'transcription_h2_annotated.tsv': ('xp2', 'h2', 'Home'),
    'transcription_h3_annotated.tsv': ('xp2', 'h3', 'Home'),
})

# Data regroups all the data, per gathered utterance.
data = None
for file_name, session_info in sessions_info.items():
    dir_path = os.path.dirname(__file__)
    file_path = os.path.join(dir_path, file_name)
    session_data = pandas.read_csv(file_path, sep='\t')
    session_data['XP'], session_data['Session'], session_data['Theme'] = session_info
    if data is None:
        data = session_data
    else:
        data = data.append(session_data)

# For the counts to work properly,
# we replace NaNs from Mis columns with an arbitrary symbol.
# 'G' stands for good.
data.Mis.fillna('G', inplace=True)

# Distinguish the two experiments
data_xp2 = data[(data.XP == 'xp2')]
data_xp3 = data[(data.XP == 'xp3')]

# Distinguish user instructions
user_data = data[(data.Speaker == 'User')]
user_data_xp2 = data_xp2[(data_xp2.Speaker == 'User')]
user_data_xp3 = data_xp3[(data_xp3.Speaker == 'User')]

# Data by session regroups statistics for each session.
data_by_session = data_xp3.groupby(['Session', 'Theme'])
data_sum_by_session = data_by_session.sum()
data_count_by_session = data_by_session.count()
# Puts "Theme" as a column rather than an index
data_sum_by_session.reset_index(level=1, inplace=True)

# Testing that the NLU is theme-independent.
#-------------------------------------------
print('--------------------------------------')
mis_theme_count = pandas.crosstab(
    user_data_xp3.Theme,
    user_data_xp3.Mis,
    margins=True)
print('Understanding vs. Theme, contingency table:\n%s'
        % mis_theme_count)
test_independence(mis_theme_count)

# Testing that the number of successfully taught behaviors
# is independent from the theme.
#-------------------------------------------
print('--------------------------------------')
bs_theme_count = pandas.crosstab(
    data_sum_by_session.Theme,
    data_sum_by_session.BS,
    margins=True)
print('Understanding vs. Taught Behaviors, contingency table:\n%s'
        % bs_theme_count)
test_independence(bs_theme_count)


# Testing the increase in misrecognition.
#--------------------------------------------
print('--------------------------------------')
xp_mis_count = pandas.crosstab(
    user_data.XP,
    user_data.Mis != 'G',
    margins=True)
print('Experiment vs. Misrecognition, contingency table:\n%s'
        % xp_mis_count)
test_independence(xp_mis_count)

# Testing the improvement of NLU.
#--------------------------------------------
print('--------------------------------------')
user_data_no_misA = user_data[user_data.Mis != 'A']
xp_nlu_mis_count = pandas.crosstab(
    user_data_no_misA.XP,
    user_data_no_misA.Mis.apply(lambda x: x in ('S', 'U')),
    margins=True)
print('Experiment vs. NLU misrecognition, contingency table:\n%s'
        % xp_nlu_mis_count)
test_independence(xp_nlu_mis_count)