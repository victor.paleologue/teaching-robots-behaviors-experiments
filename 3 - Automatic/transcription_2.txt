Theme Home
Teaching 3/5
Interaction 3/5

5156.610275 H: To approach this to move forward and say hello
5156.625887 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5156.627117 Behavior "Express when it is not understood"  suggests action "Ok"
5156.766234 R: Ok
5157.558191 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5157.575866 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5157.578120 Behavior "Express when it is not understood"  dropped action "Ok"
5157.580245 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5206.475227 H: Approach
5206.490888 Behavior "Express when it is not understood"  suggests action "Ok"
5206.491358 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5206.568498 R: Ok
5207.266922 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5207.272622 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5207.273620 Behavior "Express when it is not understood"  dropped action "Ok"
5207.275159 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5217.982137 H: Approach again
5218.190513 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5218.310952 R: Sorry, I don't understand.
5220.438778 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5224.343795 H: Approach
5224.549956 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5224.691254 R: Sorry, I don't understand.
5226.725910 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5233.464602 H: Approach
5233.672850 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5233.836105 R: Sorry, I don't understand.
5235.828556 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5254.420122 H: To dance is to move forward
5254.430343 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5254.434307 Behavior "Express when it is not understood"  suggests action "Is that all?"
5254.595703 R: Is that all?
5255.421249 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5255.432405 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5255.432797 Behavior "Express when it is not understood"  dropped action "Is that all?"
5255.434247 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5256.371158 H: Turn left
5256.386851 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0, 0, 1.57)"
5256.387667 Behavior "To obey"  suggests action "ALMotion.moveTo(0, 0, 1.57)"
5256.393652 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5256.395524 Behavior "Express when it is not understood"  suggests action "Is that all?"
5256.719700 R: Is that all?
5257.357995 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5257.358906 Behavior "Express when it is not understood"  dropped action "Is that all?"
5260.005326 Behavior "To obey"  dropped action "ALMotion.moveTo(0, 0, 1.57)"
5260.016123 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5260.016509 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0, 0, 1.57)"
5260.021740 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5285.572213 H: Turn right
5285.589372 Behavior "To obey"  suggests action "ALMotion.moveTo(0, 0, -1.57)"
5285.592513 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0, 0, -1.57)"
5285.599269 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5285.600084 Behavior "Express when it is not understood"  suggests action "Is that all?"
5285.795035 R: Is that all?
5286.589692 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5286.591674 Behavior "Express when it is not understood"  dropped action "Is that all?"
5288.958188 Behavior "To obey"  dropped action "ALMotion.moveTo(0, 0, -1.57)"
5288.972462 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5288.972569 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0, 0, -1.57)"
5288.973346 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5292.874504 H: Move forward
5292.882235 Behavior "To obey"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5292.883214 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5292.884621 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5292.887337 Behavior "Express when it is not understood"  suggests action "Is that all?"
5292.927168 Behavior "To obey"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5292.929248 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5292.930267 Behavior "Express when it is not understood"  dropped action "Is that all?"
5297.693574 H: Yeah
5297.708365 Behavior "Express when it is not understood"  suggests action "Is that all?"
5297.716750 Behavior "To learn behaviors from explanation"  suggests action "Ok, to dance is to approach that, then to move forward, then to say hello and then to move forward."
5297.716845 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5297.721258 Behavior "Express when it is not understood"  suggests action "Ok, to dance is to approach that, then to move forward, then to say hello and then to move forward."
5297.721364 Behavior "Express when it is not understood"  dropped action "Is that all?"
5297.927062 R: Ok, to dance is to approach that, then to move forward, then to say hello and then to move forward.
5304.176314 Behavior "To learn behaviors from explanation"  dropped action "Ok, to dance is to approach that, then to move forward, then to say hello and then to move forward."
5304.183703 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5304.183998 Behavior "Express when it is not understood"  dropped action "Ok, to dance is to approach that, then to move forward, then to say hello and then to move forward."
5304.191769 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5329.990107 H: Okay
5330.198684 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5330.379463 R: Sorry, I don't understand.
5332.387498 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5357.180913 H: To sway
5357.184698 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5357.185552 Behavior "Express when it is not understood"  suggests action "Ok"
5357.305731 R: Ok
5359.622477 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5359.636536 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5359.636628 Behavior "Express when it is not understood"  dropped action "Ok"
5359.643249 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5362.286298 H: Is to turn right
5362.298397 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5362.300883 Behavior "Express when it is not understood"  suggests action "Is that all?"
5362.461860 R: Is that all?
5363.260237 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5363.266827 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5363.267194 Behavior "Express when it is not understood"  dropped action "Is that all?"
5363.271819 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5366.862032 H: Is to turn left
5367.071640 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5367.269963 R: Sorry, I don't understand.
5369.219418 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5375.508158 H: Turn right
5375.516857 Behavior "To obey"  suggests action "ALMotion.moveTo(0, 0, -1.57)"
5375.517413 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0, 0, -1.57)"
5375.526855 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5375.528778 Behavior "Express when it is not understood"  suggests action "Is that all?"
5375.699503 R: Is that all?
5376.471598 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5376.472457 Behavior "Express when it is not understood"  dropped action "Is that all?"
5378.956893 Behavior "To obey"  dropped action "ALMotion.moveTo(0, 0, -1.57)"
5378.963877 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5378.963968 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0, 0, -1.57)"
5378.966863 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5384.729630 H: Turn left
5384.739266 Behavior "To obey"  suggests action "ALMotion.moveTo(0, 0, 1.57)"
5384.741974 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5384.742917 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0, 0, 1.57)"
5384.749340 Behavior "Express when it is not understood"  suggests action "Is that all?"
5384.942303 R: Is that all?
5385.744572 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5385.747192 Behavior "Express when it is not understood"  dropped action "Is that all?"
5388.105946 Behavior "To obey"  dropped action "ALMotion.moveTo(0, 0, 1.57)"
5388.112517 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5388.112611 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0, 0, 1.57)"
5388.113089 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5392.390909 H: Say
5392.399405 Behavior "To obey"  suggests action "What do I have to say?"
5392.410929 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5392.413128 Behavior "Express when it is not understood"  suggests action "What do I have to say?"
5392.419144 Behavior "Express when it is not understood"  suggests action "Is that all?"
5392.640786 R: What do I have to say?
5393.984872 Behavior "To obey"  dropped action "What do I have to say?"
5393.987586 Behavior "Express when it is not understood"  dropped action "What do I have to say?"
5394.092398 R: Is that all?
5394.931107 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5394.938275 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5394.938365 Behavior "Express when it is not understood"  dropped action "Is that all?"
5394.941936 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5398.400457 H: I am swaying
5398.410973 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5398.413726 Behavior "Express when it is not understood"  suggests action "Is that all?"
5398.602167 R: Is that all?
5399.370796 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5399.391251 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5399.391537 Behavior "Express when it is not understood"  dropped action "Is that all?"
5399.397445 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5400.905274 H: Yes
5400.924073 Behavior "To learn behaviors from explanation"  suggests action "Ok, to sway is to turn right."
5400.925148 Behavior "Express when it is not understood"  suggests action "Ok, to sway is to turn right."
5401.108433 R: Ok, to sway is to turn right.
5403.566877 Behavior "To learn behaviors from explanation"  dropped action "Ok, to sway is to turn right."
5403.602846 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5403.603310 Behavior "Express when it is not understood"  dropped action "Ok, to sway is to turn right."
5403.604164 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5420.662308 H: Okay
5420.872965 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5421.064753 R: Sorry, I don't understand.
5423.036389 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5427.543558 H: Please sway
5427.552251 Behavior "To obey"  suggests action "ALMotion.moveTo(0, 0, -1.57)"
5427.553584 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0, 0, -1.57)"
5430.957293 Behavior "To obey"  dropped action "ALMotion.moveTo(0, 0, -1.57)"
5430.964957 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5430.965351 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0, 0, -1.57)"
5430.966705 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5435.531441 H: Please turn left
5435.546179 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0, 0, 1.57)"
5435.548543 Behavior "To obey"  suggests action "ALMotion.moveTo(0, 0, 1.57)"
5438.924644 Behavior "To obey"  dropped action "ALMotion.moveTo(0, 0, 1.57)"
5438.934805 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5438.935575 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0, 0, 1.57)"
5438.942140 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5449.760231 H: To leave
5449.764988 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5449.766926 Behavior "Express when it is not understood"  suggests action "Ok"
5449.929871 R: Ok
5450.569742 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5450.577412 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5450.577503 Behavior "Express when it is not understood"  dropped action "Ok"
5450.578297 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5454.338646 H: Is to move forward
5454.349584 Behavior "Express when it is not understood"  suggests action "Is that all?"
5454.352451 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5454.491126 R: Is that all?
5455.304157 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5455.311198 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5455.311291 Behavior "Express when it is not understood"  dropped action "Is that all?"
5455.311711 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5461.928496 H: No
5461.931920 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5461.934282 Behavior "Express when it is not understood"  suggests action "Ok"
5462.100873 R: Ok
5462.789100 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5462.796359 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5462.796643 Behavior "Express when it is not understood"  dropped action "Ok"
5462.797574 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5467.420920 H: Move forward
5467.428986 Behavior "To obey"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5467.434323 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5467.453406 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5467.454078 Behavior "Express when it is not understood"  suggests action "Ok"
5467.474952 Behavior "To obey"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5467.475500 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5467.664596 R: Ok
5468.319183 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5468.338363 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5468.338456 Behavior "Express when it is not understood"  dropped action "Ok"
5468.339094 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5475.121800 H: Say
5475.127158 Behavior "To obey"  suggests action "What do I have to say?"
5475.127831 Behavior "Express when it is not understood"  suggests action "What do I have to say?"
5475.172662 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5475.175272 Behavior "Express when it is not understood"  suggests action "Ok"
5475.325950 R: What do I have to say?
5476.641870 Behavior "To obey"  dropped action "What do I have to say?"
5476.643486 Behavior "Express when it is not understood"  dropped action "What do I have to say?"
5476.784398 R: Ok
5477.424806 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5477.435543 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5477.436081 Behavior "Express when it is not understood"  dropped action "Ok"
5477.440491 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5480.989396 H: Say
5480.995064 Behavior "To obey"  suggests action "What do I have to say?"
5480.996617 Behavior "Express when it is not understood"  suggests action "What do I have to say?"
5481.018638 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5481.022646 Behavior "Express when it is not understood"  suggests action "Ok"
5481.169158 R: What do I have to say?
5482.526352 Behavior "To obey"  dropped action "What do I have to say?"
5482.527720 Behavior "Express when it is not understood"  dropped action "What do I have to say?"
5482.527807 Behavior "Express when it is not understood"  dropped action "Ok"
5482.684570 R: Ok
5485.773076 H: I'm leaving this room
5485.780290 Behavior "Express when it is not understood"  suggests action "Ok"
5485.825937 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5485.826067 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5485.907978 Behavior "Express when it is not understood"  suggests action "Ok"
5485.908260 Behavior "Express when it is not understood"  dropped action "Ok"
5486.546544 R: Ok
5486.675432 R: Ok
5487.397951 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5487.405498 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5487.405591 Behavior "Express when it is not understood"  dropped action "Ok"
5487.406140 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5492.655256 H: That's all
5492.863590 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5493.069242 R: Sorry, I don't understand.
5495.047783 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5496.829253 H: Okay
5497.035653 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5497.218041 R: Sorry, I don't understand.
5499.195411 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5511.033101 H: Please
5511.235179 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5511.447147 R: Sorry, I don't understand.
5513.432454 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5516.472605 H: Please leave
5516.688457 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5516.889729 R: Sorry, I don't understand.
5518.855989 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5522.647676 H: Leave
5522.855426 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5523.045430 R: Sorry, I don't understand.
5525.034626 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5528.213252 H: To leave
5528.225408 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5528.225510 Behavior "Express when it is not understood"  suggests action "Ok"
5528.387363 R: Ok
5529.043406 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5529.050659 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5529.051180 Behavior "Express when it is not understood"  dropped action "Ok"
5529.051657 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5533.838790 H: Is to move forward
5533.847497 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5533.852459 Behavior "Express when it is not understood"  suggests action "Is that all?"
5534.004252 R: Is that all?
5534.787766 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5534.796663 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5534.797942 Behavior "Express when it is not understood"  dropped action "Is that all?"
5534.799224 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5539.038986 H: Move forward
5539.045540 Behavior "To obey"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5539.049927 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5539.053810 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5539.054415 Behavior "Express when it is not understood"  suggests action "Is that all?"
5539.082440 Behavior "To obey"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5539.089174 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5539.098109 Behavior "Express when it is not understood"  dropped action "Is that all?"
5539.267671 R: Is that all?
5542.570082 H: Say
5542.570520 Behavior "Express when it is not understood"  suggests action "Is that all?"
5542.578429 Behavior "To obey"  suggests action "What do I have to say?"
5542.579825 Behavior "Express when it is not understood"  suggests action "What do I have to say?"
5542.591464 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5542.591557 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5542.592662 Behavior "Express when it is not understood"  suggests action "Is that all?"
5542.592760 Behavior "Express when it is not understood"  dropped action "Is that all?"
5542.817132 R: What do I have to say?
5544.117945 Behavior "To obey"  dropped action "What do I have to say?"
5544.119229 Behavior "Express when it is not understood"  dropped action "What do I have to say?"
5544.315173 R: Is that all?
5545.034474 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5545.045189 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5545.045281 Behavior "Express when it is not understood"  dropped action "Is that all?"
5545.047471 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5553.711240 H: I am leaving this room
5553.721113 Behavior "To learn behaviors from explanation"  suggests action "Is that all?"
5553.721948 Behavior "Express when it is not understood"  suggests action "Is that all?"
5553.875991 R: Is that all?
5554.663636 Behavior "To learn behaviors from explanation"  dropped action "Is that all?"
5554.673790 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5554.673886 Behavior "Express when it is not understood"  dropped action "Is that all?"
5554.675270 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5560.640621 H: No
5560.646524 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5560.647380 Behavior "Express when it is not understood"  suggests action "Ok"
5560.849659 R: Ok
5561.450852 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5561.460458 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5561.460933 Behavior "Express when it is not understood"  dropped action "Ok"
5561.465690 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5572.380599 H: Okay
5572.386674 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5572.387656 Behavior "Express when it is not understood"  suggests action "Ok"
5572.565882 R: Ok
5573.246679 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5573.257552 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5573.258064 Behavior "Express when it is not understood"  dropped action "Ok"
5573.261247 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5581.207267 H: Please leave
5581.218388 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5581.220773 Behavior "Express when it is not understood"  suggests action "Ok"
5581.472089 R: Ok
5582.143742 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5582.153784 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5582.153871 Behavior "Express when it is not understood"  dropped action "Ok"
5582.154402 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5592.310803 H: Please leave
5592.338406 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5592.341462 Behavior "Express when it is not understood"  suggests action "Ok"
5592.536181 R: Ok
5593.204914 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5593.210471 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5593.210986 Behavior "Express when it is not understood"  dropped action "Ok"
5593.216353 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5605.462913 H: Please sway
5605.469713 Behavior "To obey"  suggests action "ALMotion.moveTo(0, 0, -1.57)"
5605.471845 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0, 0, -1.57)"
5605.500803 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5605.501778 Behavior "Express when it is not understood"  suggests action "Ok"
5605.697953 R: Ok
5606.343887 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5606.344812 Behavior "Express when it is not understood"  dropped action "Ok"
5608.818327 Behavior "To obey"  dropped action "ALMotion.moveTo(0, 0, -1.57)"
5608.825230 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5608.825321 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0, 0, -1.57)"
5608.828944 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5617.300946 H: Turn left
5617.310706 Behavior "To obey"  suggests action "ALMotion.moveTo(0, 0, 1.57)"
5617.312135 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0, 0, 1.57)"
5617.355177 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5617.356300 Behavior "Express when it is not understood"  suggests action "Ok"
5617.534146 R: Ok
5618.183163 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5618.185171 Behavior "Express when it is not understood"  dropped action "Ok"
5620.692434 Behavior "To obey"  dropped action "ALMotion.moveTo(0, 0, 1.57)"
5620.705974 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5620.706791 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0, 0, 1.57)"
5620.709220 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5622.769032 H: Move forward
5622.777102 Behavior "To obey"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5622.778359 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5622.822725 Behavior "To obey"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5622.824380 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5622.844804 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5630.494980 H: Move forward
5630.503550 Behavior "Express when it is not understood"  suggests action "Ok"
5630.505329 Behavior "To obey"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5630.509576 Behavior "Express when it is not understood"  suggests action "ALMotion.moveTo(0.5, 0, 0)"
5630.552702 Behavior "To obey"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5630.557789 Behavior "Express when it is not understood"  dropped action "ALMotion.moveTo(0.5, 0, 0)"
5630.627598 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5630.627685 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5630.738431 Behavior "Express when it is not understood"  suggests action "Ok"
5630.738518 Behavior "Express when it is not understood"  dropped action "Ok"
5631.339156 R: Ok
5631.564786 R: Ok
5632.208718 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5632.219981 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5632.220129 Behavior "Express when it is not understood"  dropped action "Ok"
5632.223248 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5653.549751 H: Okay
5653.558420 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5653.559080 Behavior "Express when it is not understood"  suggests action "Ok"
5653.743467 R: Ok
5654.375537 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5654.385612 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5654.386248 Behavior "Express when it is not understood"  dropped action "Ok"
5654.399798 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5660.370863 H: To
5660.377106 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5660.378255 Behavior "Express when it is not understood"  suggests action "Ok"
5660.644155 R: Ok
5661.237851 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5661.245162 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5661.245881 Behavior "Express when it is not understood"  dropped action "Ok"
5661.252812 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5697.568100 H: To check
5697.583699 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5697.584156 Behavior "Express when it is not understood"  suggests action "Ok"
5697.784269 R: Ok
5698.422940 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5698.431401 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5698.431494 Behavior "Express when it is not understood"  dropped action "Ok"
5698.440703 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5701.253873 H: Is to raise
5701.459666 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5701.686138 R: Sorry, I don't understand.
5703.612889 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5704.494376 H: The right arm
5704.702772 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5704.883349 R: Sorry, I don't understand.
5706.879771 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5713.052850 H: To check
5713.058587 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5713.060332 Behavior "Express when it is not understood"  suggests action "Ok"
5713.286430 R: Ok
5713.963915 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5713.974593 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5713.974684 Behavior "Express when it is not understood"  dropped action "Ok"
5713.975337 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5716.570736 H: Raise the right arm
5716.580501 Behavior "To obey"  suggests action "ALMotion.angleInterpolation("RShoulderPitch", -1.6, 3, 1)"
5716.582399 Behavior "Express when it is not understood"  suggests action "ALMotion.angleInterpolation("RShoulderPitch", -1.6, 3, 1)"
5719.621213 Behavior "To obey"  dropped action "ALMotion.angleInterpolation("RShoulderPitch", -1.6, 3, 1)"
5719.630352 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5719.630444 Behavior "Express when it is not understood"  dropped action "ALMotion.angleInterpolation("RShoulderPitch", -1.6, 3, 1)"
5719.630968 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5726.713135 H: Lowered right arm
5726.924758 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5727.184121 R: Sorry, I don't understand.
5729.060168 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5733.113780 H: To check
5733.119997 Behavior "To learn behaviors from explanation"  suggests action "Ok"
5733.120529 Behavior "Express when it is not understood"  suggests action "Ok"
5733.357138 R: Ok
5733.969755 Behavior "To learn behaviors from explanation"  dropped action "Ok"
5733.981383 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5733.981486 Behavior "Express when it is not understood"  dropped action "Ok"
5733.986062 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5736.666912 H: Raise the right arm
5736.679725 Behavior "To obey"  suggests action "ALMotion.angleInterpolation("RShoulderPitch", -1.6, 3, 1)"
5736.683367 Behavior "Express when it is not understood"  suggests action "ALMotion.angleInterpolation("RShoulderPitch", -1.6, 3, 1)"
5739.726619 Behavior "To obey"  dropped action "ALMotion.angleInterpolation("RShoulderPitch", -1.6, 3, 1)"
5739.741183 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5739.741278 Behavior "Express when it is not understood"  dropped action "ALMotion.angleInterpolation("RShoulderPitch", -1.6, 3, 1)"
5739.741793 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5742.074445 H: And lowered the right arm
5742.282724 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5742.507873 R: Sorry, I don't understand.
5744.503657 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5846.321784 H: What is to sway
5846.333308 Behavior "To introspect a behavior"  suggests action "To sway is to turn right."
5846.334309 Behavior "Express when it is not understood"  suggests action "To sway is to turn right."
5846.748183 R: To sway is to turn right.
5848.396971 Behavior "To introspect a behavior"  dropped action "To sway is to turn right."
5848.407086 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5848.408101 Behavior "Express when it is not understood"  dropped action "To sway is to turn right."
5848.411883 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5872.282244 H: What is to leave
5872.534270 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5872.855839 R: Sorry, I don't understand.
5874.732235 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5885.640739 H: Check
5885.851269 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5886.069831 R: Sorry, I don't understand.
5887.986758 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5889.583393 H: What is to check
5889.794295 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5890.015705 R: Sorry, I don't understand.
5891.946279 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
5919.555927 H: What is to dance
5919.576751 Behavior "To introspect a behavior"  suggests action "To dance is to approach that, then to move forward, then to say hello and then to move forward."
5919.577412 Behavior "Express when it is not understood"  suggests action "To dance is to approach that, then to move forward, then to say hello and then to move forward."
5919.979874 R: To dance is to approach that, then to move forward, then to say hello and then to move forward.
5925.382796 Behavior "To introspect a behavior"  dropped action "To dance is to approach that, then to move forward, then to say hello and then to move forward."
5925.390702 Behavior "Express when it is not understood"  suggests action "Sorry, I don't understand."
5925.390791 Behavior "Express when it is not understood"  dropped action "To dance is to approach that, then to move forward, then to say hello and then to move forward."
5925.394503 Behavior "Express when it is not understood"  dropped action "Sorry, I don't understand."
