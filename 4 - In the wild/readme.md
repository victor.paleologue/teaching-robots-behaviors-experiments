# Experiment #4

## Background

This experiment follows the one published in
Victor Paléologue, Jocelyn Martin, Amit Kumar Pandey, et Mohamed Chetouani.
« Semantic-based interaction for teaching robot behavior compositions using spoken language ».
In International Conference on Social Robotics, 421–430. Springer, 2018.

It consisted in demonstrating a proof-of-concept system
allow Pepper to be taught new behaviors.
It also proposed a list of measures
to evaluate the success of the teaching interaction.

The next experiment will bring this system *in the wild*,
to check whether people actually living with robots can teach the robot new behaviors.
Pepper@Home is a programme of beta-testing of applications
driven by SoftBank Robotics Europe.
Around 20 employees live with a Pepper robot at home,
and have signed up to test applications,
and participate to various experiments.
We selected this ground for this experiment.

Pepper@Home robots are designed for other uses apart from the teaching.
As predicted in previous experiment,
the algorithms for supporting the teaching should be designed in a way
to collaborate with other, potentially conflicting, algorithms.

Moreover, not all users from the Pepper@Home population speak English:
they are mostly French, and our interaction must support that language too,
in addition to English.

All Pepper@Home participants have contractually accepted
to be recorded in order to improve the software they test.

## Objectives

Introduce the teaching algorithms (A) in a robot
that already have pre-existing interaction algorithms (B).
Let them compete within the same application (C), and:

- compare A to previous experiment to check:
  - whether the teaching remains successful,
  - how the real-world environment impacts the previous measurements;
- compare A to C to check:
  - whether the teaching remains successful,
  - how the competition impacts the overall usability;
- compare B to C to check:
  - whether the pre-existing interaction remains successful,
  - how the competition impacts the overall usability;
- study openly C to check:
  - if and what participants effectively teach in their everyday life.

With the help of recordings of the experiment from the robot's point of view,
we will also gather data to study:

- what users actually said, vs. what was understood,
- what the users actually wanted to teach,
- in which situations the interaction fails,
- how the users try to repair the interaction,
- how the users appeared to feel during the interaction.

## Setup

### Android application

An Android app named DEF provides
the pre-existing interaction algorithms deployed on Pepper@Home robots.
These algorithms are be considered the **baseline content**.
Pepper@Home robots are Pepper robots (1.8a, running NAOqi 2.9.1)
and are deployed at the participants' home.
The content is available at least in English and in French.

![App seen from the launcher](screen app launcher.png)

![Application's main screen](screen main.png)

DEF is an adaptation of
an existing application called ABC,
that provided the baseline interaction behaviors.
DEF provides the teaching behaviors
in addition to the baseline content.
It has a similar interface,
and is made distinguishable
only by the colors it shows.
The new content can be enabled optionally.
All the contents are available in French and in English.
Users can set their preferred language in the robot's settings.

In more details, the options are:

- chat mode: standard chat from the service Conversation vs. SemanticAgent's special chat
  with action suggestions and selection.
- algorithm selection: in SemanticAgent's chat, choose the enabled interaction algorithms among:
  - baseline content
  - obey
  - learn behavior composition
  - behavior introspection
  - tell if not understood.

We added the possibility to record the interaction when needed for the experiment.
When the recording is used, a privacy notice is shown,
and the user agreement can be requested (1).
In addition an indicator should recall the users that the recording is ongoing.

![App requests agreement](screen agreement.png)

Two parameters of this user agreement are possible:

- sources: audio only or audio and video
- agreement, required or optional: if required, the next phase cannot be performed without user's agreement.

The updated ABC have an additional launchable activity, named in English "Teaching Experiment". When this activity is run the experiment protocol is unrolled, leaving no control to the user over the options.

DEF has only access to the verbal exchanges
that occurs during the teaching,
and of the behaviors that are learned.
Whenever it can, it sends the data
to an online server.

### NAOqi

TODO: provide image version
TODO: provide packages and version numbers

We deploy a custom version of the semantic package
(a package of NAOqi providing semantic analysis)
to that provides a service called "SemanticAgent".
The SemanticAgent provides a custom Chat action
that puts together an arbitrary list of chatbots
with the hard-coded behaviors of the experiment.

We also deploy an internal package called
"Cognitive Services", that provides a fallback
for speech recognition.
If SBRE's speech recognition service went down,
it would use Microsoft Cognitive Services
to perform speech-to-text.

We also deploy an internal package called
"Multimodal Recorder",
that performs the recording during the interaction.

### Online Collection

We use a secure DAV server to collect the data.
The applications deployed can only send data,
and have no access back to what was sent.
The only persons having access to the server
to collect the data are Victor Paléologue,
the experimenter,
and the server administrator of SBRE.

### Installation instructions

TODO: provide e-mail content

## Protocol

Users are informed of the deployment of DEF.
They are asked to install it,
and then to run it,
and to not leave the application
until the end of the experiment.
If they leave,
the app will try
to resume the experiment
at wherever it was left, but may fail.
Incomplete experiments will still be collected,
so that to be aware of their existence,
but will not contribute to the statistical results.

### 1. Baselines

The two first phases are supposed to be independent, and should be tested in a random order.

### a) Classic Chat

In this phase we evaluate the baseline interaction with the robot in Pepper@Home. No recording is required.

The robot is configured to use the baseline content on a standard chat, in the current language of the robot.

They are asked to exchange with the robot at least 30 times. When done, the interaction stops and they are directed to a questionnaire, that includes an evaluation on the [System Usability Scale (SUS)](https://www.usability.gov/how-to-and-tools/methods/system-usability-scale.html), and extra optional input:

 * What issues did you encounter during this phase? What could be improved?

 * Other comments:

Questions order is randomized. When done, the app switches to the next phase.

### 1.b) Teaching behaviors only

In this phase, we evaluate the teaching algorithms, in a similar way to how it was done in the two previous experiments. This phase require the audio to be recorded. If the participant disagrees, the experiment is interrupted.

The second phase consists in showing equivalent instructions as the sheets shown in previous studies. A vocal explanation is given by the robot. A button lets the user request the instructions again. When they start speaking to the robot, a 10 minutes countdown starts. They have to compose as many behaviors as possible.

![Instructions for teaching behaviors](screen pure teaching instructions.png)

After the 10 minutes are passed, the screen switches to the questionnaire, with the classic chat's questionnaire, plus the same questions as in previous experiments:

- whether they thought that the teaching was successful, on the Likert scale.
- whether the overall interaction was easy, on the Likert scale.

Questions order is randomized.

From the collected data, we will compute:

- The number of utterances (or instructions) provided by the users (IC).

- The average rate of instructions misrecognized (Mis%).

- The average time in seconds needed to teach a behavior, including the time lost with misrecognized instructions or any other error (AT w Err).

- The percentage of behaviors that were successfully taught, among the total number of behaviors attempted to be taught (TS%).

- The percentage of users who managed to teach behaviors (UTS%).

- The percentage of users who managed to teach composite behaviors (UTSC%).

- How successful the teaching felt. It is the Experienced Teaching Success (ETS).

- How easy the interaction felt. It is the Experienced Interaction Ease (EIE).

### 2. Interaction augmented with teaching

In that third phase, participants are recorded in video. The app is configured to mix the baseline interaction with the teaching, using the SemanticAgent's chat.

The robot and the app tells
to the participant that
he or she is required
to teach two behaviors to the robot,
while the robot supports the baseline interaction.
When the participant has taught the two behaviors,
the last questionnaire appear,
mixing questionnaires from phases 1 and 2.

### 3. In the wild

The controlled experiment is now over, but the application remains available. When run again, the first time, it is asked whether to enable the teaching or not. It is also told how to change this later on (through a setting).

If the teaching is activated, the app behaves like in phase 3, but with no questionnaire at the end.

Online, according to Pepper@Home's cycles, we will be able to ask the following questions:

- do you feel more connected to your robot now?
- do you think it is more worth interacting with the robot now?
- what behavior did you refrain to teach?
- what behavior would you have liked the robot to know already?
- any other comment on this feature?

Questions order is randomized.

## Data Collection

Pepper@Home users have already accepted that personal data will be collected to be used for the improvement of SoftBank Robotics Europe (SBRE)'s products, only. To publish the data, the user consentment is still required!

The text, audio and video will be collected using NAOqi. A service in NAOqi can take care of this in the background. File names will include a unique identifier of the software installation, of the interaction session, the phase number and a datetime.

The data will be uploaded to an endpoint managed by SBRE, so that to be made available for this research.

> Participants authorized SBRE to record them within the Pepper@Home program only, and for internal use only: the data cannot be published outside without explicit consentment of the participants.

TODO: INFORM ABOUT THE TIME LIMIT BECAUSE OF SPACE ON DEVICE.

## Population

Pepper@Home population is composed of a couple dozens of SBRE employees. They are already trained for using Pepper.
Each of them have a Pepper 1.8a at home, and interact with it on from time to time (SPECIFY FREQUENCY HERE).

## Analysis

Incomplete recordings were gathered.
Some of them are transcribed in `dialogues.tsv`.
It is an array of tab-separated values,
that can be read as follows:

- Session: the ID of the experimental session.
  One session corresponds to a run of the teaching
  experiment activity.
- Phase: the ID of the phase within the experimental session,
  among `pure_teaching` or `teaching_chat`.
- Time: the time stamp shown in the record,
  counting the time since the beginning of the record,
  in seconds, rounded to the centisecond.
- Speaker: `H` stands for human user,
  `R` stands for robot.
- Utterance: the transcription of the user's utterance
  by the experimenter, considered perfect.
- NLU: indicates how the NLU performed,
  including the ASR.
  `N` stands as not heard:
  the ASR did not respond at all;
  `A` stands for ASR errors;
  `S` stands for semantic extraction error;
  `U` stands for unsupported by the behaviors;
  `F` stands for execution failure;
  `G` stands as good: it went well.
- BS: 1 when a behavior teaching is successful.
- Comment: extra column for arbitrary comments.
