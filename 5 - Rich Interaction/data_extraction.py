#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import datetime
import dateutil
import functools
import os
import pandas
import re


def check_str_collection_ends_and_simplify(input, start_delimiter, end_delimiter):
    if input[0] != start_delimiter:
        raise RuntimeError("Input not starting with %s" % start_delimiter)
    if input[-1] != end_delimiter:
        raise RuntimeError("Input not ending with %s" % end_delimiter)
    input = input[1:-1]
    if len(input) > 0:
        input = input + ', '
    return input


def parse_android_preferences_string(data_str):
    '''
    Parses a dict from a dump of Android preferences,
    produced using preferences.toString().
    '''
    # Android preferences.toString()
    # produces data is not rigorously respecting
    # a parseable syntax.
    # We use empirical regex to extract the data (sorry).

    # New lines are removed because
    # they are not meaningful and disturb regex.
    data_str = data_str.replace('\n', ' ')

    # TTS tags are removed because
    # they are not meaningful and disturb our regex.
    data_str = re.sub(r"\\rspd=[0-9]+?\\", "", data_str)
    data_str = re.sub(r"\\pau=[0-9]+?\\", "", data_str)
    data_str = re.sub(r"\\mrk=[0-9]+?\\", "", data_str)

    # This is what we should know how to rebuild.
    data_to_rebuild = data_str

    # The data is an associative array, delimited by "{}",
    # and separated with ", ".
    # To make the regex work easy,
    # we make every entry look alike, be continued by ", ".
    data_str = check_str_collection_ends_and_simplify(data_str, '{', '}')

    # We identify keys and transform the data
    # surrounding it to make it easier to parse.
    data_str = re.sub(
        r"(\w+?)=", "\nkey: \\1\nvalue: ", data_str)
    assert(data_str[0] == '\n')
    data_str = data_str[1:]

    # The data is now presented as follows:
    # key: <key>\n
    # value: <value>, \n
    # ...
    data_lines = data_str.splitlines()
    data = dict()
    key = None
    for data_line in data_lines:
        if key is None:
            if not data_line.startswith('key: '):
                raise RuntimeError('Expected a key, got: %s' % data_line)
            key = data_line[5:]
        else:
            if not data_line.startswith('value: '):
                raise RuntimeError('Expected a value, got: %s' % data_line)
            if not data_line.endswith(', '):
                raise RuntimeError('Expected line to end with ", ", it does not: %s' % data_line)
            data[key] = data_line[7:-2]
            key = None

    # Rebuild the data as a double-check that nothing was lost.
    rebuilt = str('{')
    for (key, value) in data.items():
        rebuilt += key + '=' + value + ', '
    rebuilt = rebuilt[:-2] + '}'
    assert(data_to_rebuild == rebuilt)
    return data


def data_dict_from_file(file_path):
    '''
    Parses a dict from a dump file of Android preferences.
    '''
    file_base = os.path.splitext(file_name)[0]
    session = file_base.partition('_')[0]
    with open(file_path, 'r') as file:
        session_data = parse_android_preferences_string(file.read())
        session_data['session'] = session
        return session_data


# The data about sessions, at a coarse grain.
data_sessions = pandas.DataFrame()

this_dir_path = os.path.dirname(__file__)
data_dir_path = os.path.join(this_dir_path, 'text_data')

for root, dirs, file_paths in os.walk(data_dir_path):
    for file_name in file_paths:
        if file_name.endswith('.txt'):
            print('Found data file: %s' % file_name)
            session_data = data_dict_from_file(os.path.join(root, file_name))
            data_sessions = data_sessions.append(session_data, ignore_index=True)
        else:
            print('Ignored non-data file: %s' % file_name)

print(data_sessions)


def iso_datetime_str_to_second_delta(origin_datetime, iso_datetime_str):
    datetime = dateutil.parser.isoparse(iso_datetime_str)
    delta = datetime - origin_datetime
    return delta.total_seconds()


def extract_dialogue(session, phase, history_str):
    dialogue = pandas.DataFrame(
        columns=[
            'session',
            'phase',
            'time',
            'speaker',
            'transcription'
        ]
    )

    # History is a serialized array of entries.
    # We remove the ends and transform it
    # to process it easily with regex.
    history_str = check_str_collection_ends_and_simplify(history_str, '[', ']')

    matches = re.findall(
        r"(.*?) (H|R): \"(.*?)\", ",
        history_str)

    for match in matches:
        entry = dict()
        entry['session'] = session
        entry['phase'] = phase
        entry['time'] = match[0]
        entry['speaker'] = match[1]
        entry['transcription'] = match[2]
        dialogue = dialogue.append(entry, ignore_index=True)

    rebuilt_history_txt = str()
    for _, entry in dialogue.iterrows():
        rebuilt_history_txt += '%s %s: "%s", ' % (
                entry['time'],
                entry['speaker'],
                entry['transcription']
            )

    assert(rebuilt_history_txt == history_str)

    time_origin = dialogue['time'].min()

    if isinstance(time_origin, str):
        time_origin = dateutil.parser.isoparse(time_origin)
        compute_time = functools.partial(
            iso_datetime_str_to_second_delta, time_origin)
        dialogue['time'] = dialogue['time'].apply(compute_time)
    return dialogue.sort_values(by=['time'])


data_dialogues = pandas.DataFrame(
    columns=[
        'session',
        'phase',
        'time',
        'speaker',
        'transcription'
    ]
)


def extract_dialogues(data_session):
    global data_dialogues

    session = data_session.loc['session']
    dialogues = {
        'classic_chat': data_session.loc['classic_chat_history'],
        'pure_teaching': data_session.loc['pure_teaching_history'],
        'teaching_chat': data_session.loc['teaching_chat_history']
    }

    for (phase, history) in dialogues.items():
        if not isinstance(history, str) or len(history) < 2:
            continue
        dialogue = extract_dialogue(session, phase, history)
        data_dialogues = data_dialogues.append(dialogue, ignore_index=True)


data_sessions.apply(extract_dialogues, axis=1)
print(data_dialogues)

# Now let's extract learned behaviors.
data_behaviors = pandas.DataFrame(
    columns=[
        'session',
        'phase',
        'label',
        'recipe'
    ]
)

for _, session_data in data_sessions.iterrows():
    entry = dict()
    entry['session'] = session_data['session']
    entry['phase'] = 'pure_teaching'

    # Behaviors is a list of pairs:
    # [{"<label>", "[<elt>, ...]"}, ...]
    # We remove the ends and transform it
    # to process it easily with regex.
    behaviors = session_data['pure_teaching_learned_behaviors']
    behaviors = check_str_collection_ends_and_simplify(behaviors, '[', ']')

    behavior_matches = re.findall(
        "\\{ \"(.*?)\", \"\\[(.*?)\\]\"\\}, ",
        behaviors)

    for behavior_match in behavior_matches:
        entry['label'] = behavior_match[0]
        entry['recipe'] = behavior_match[1].split(', ')
        data_behaviors = data_behaviors.append(entry, ignore_index=True)

    entry['phase'] = 'teaching_chat'
    behaviors = session_data['teaching_chat_learned_behaviors']
    if not isinstance(behaviors, str) or len(behaviors) < 2:
        continue
    behaviors = check_str_collection_ends_and_simplify(behaviors, '[', ']')

    behavior_matches = re.findall(
        "\\{ \"(.*?)\", \"\\[(.*?)\\]\"\\}, ",
        behaviors)

    for behavior_match in behavior_matches:
        entry['label'] = behavior_match[0]
        entry['recipe'] = behavior_match[1].split(', ')
        data_behaviors = data_behaviors.append(entry, ignore_index=True)


print(data_behaviors)


sessions_path = os.path.join(this_dir_path, 'extracted_sessions.tsv')
data_sessions.to_csv(path_or_buf=sessions_path, sep='\t', index=False)

dialogues_path = os.path.join(this_dir_path, 'extracted_dialogues.tsv')
data_dialogues.to_csv(path_or_buf=dialogues_path, sep='\t', index=False)

behaviors_path = os.path.join(this_dir_path, 'extracted_behaviors.tsv')
data_behaviors.to_csv(path_or_buf=behaviors_path, sep='\t', index=False)