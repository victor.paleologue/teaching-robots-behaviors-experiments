import functools
import math
import os
import pandas
import re
import scipy.stats
import matplotlib.pyplot as plt


print('-----------------------------------------------------')
dir_path = os.path.dirname(__file__)


# All the dialogues, with our annotations.
annotated_dialogues_file_path = os.path.join(dir_path, 'extracted_dialogues_annotated.tsv')
dialogues = pandas.read_csv(annotated_dialogues_file_path, sep='\t')
print('Dialogues of XP 5:')
print(dialogues)
print('-----------------------------------------------------')

nof_participants = len(dialogues.session.unique())
print('Number of participants: %s' % nof_participants)
print('-----------------------------------------------------')

# All the taught behaviors.
behaviors_file_path = os.path.join(dir_path, 'extracted_behaviors_annotated.tsv')
behaviors = pandas.read_csv(behaviors_file_path, sep='\t')


# Consistency check:
# the number of successful teachings
# should be the same between the annotation
# and the collected data.
nof_behaviors_annotated = (dialogues['bs'] == 'G').sum()
print('Number of behaviors found in annotations: %s' % nof_behaviors_annotated)

teaching_successes = dialogues[(dialogues.bs == 'G')]
nof_teaching_successes = teaching_successes.groupby(['session', 'phase']).count().bs


nof_behaviors_collected = len(behaviors.index)
print('Number of behaviors actually collected: %s' %
nof_behaviors_collected)

nof_taught_behaviors = behaviors.groupby(['session', 'phase']).count().label

print('Learned behaviors:')
print(behaviors)


def parse_recipe(recipe_str):
    if not isinstance(recipe_str, str) and math.isnan(recipe_str):
        return list()

    start_delimiter = '['
    end_delimiter = ']'
    if recipe_str[0] != start_delimiter:
        raise RuntimeError("Input not starting with %s" % start_delimiter)
    if recipe_str[-1] != end_delimiter:
        raise RuntimeError("Input not ending with %s" % end_delimiter)
    recipe_str = recipe_str[1:-1]
    if len(recipe_str) > 0:
        recipe_str = recipe_str + ', '
    matches = re.findall(r"'(.*?)', ", recipe_str)
    return list(matches)


behaviors['recipe_obj'] = behaviors['recipe'].transform(lambda recipe: parse_recipe(recipe))
behaviors['expected_recipe_obj'] = behaviors['expected_recipe'].transform(lambda recipe: parse_recipe(recipe))


def sequence_lev_ratio(actual, expected):
    import editdistance
    distance = editdistance.eval(actual, expected)
    ratio = float(distance) / float(max(len(actual), len(expected)))
    return 100 * ratio

behaviors['LRt'] = behaviors[['recipe_obj', 'expected_recipe_obj']].aggregate(lambda row: sequence_lev_ratio(row['recipe_obj'], row['expected_recipe_obj']), axis=1)


# It appears the numbers differ:
# - we're missing one full phase in the annotated data,
#   because we're missing the replies of the robot.
# - some behaviors may have been learned silently,
#   if somehow the learning behavior failed to notify the user.
# The real number of behaviors is the "collected" one.
# However, for time-to-teach computations,
# we must rely on the list of annotated behaviors.
# Try not to mix the numbers by mistake!
print('-----------------------------------------------------')

# Now let's produce the usual measurements.
# First we build the top data frame,
# regrouping the data per session + phase.
sessions_file_path = os.path.join(dir_path, 'extracted_sessions.tsv')
sessions = pandas.read_csv(sessions_file_path, sep='\t')
session_phases_columns = set(['Phase'])

def remove_prefix_in(prefixes, string):
    for prefix in prefixes:
        if string.startswith(prefix):
            return string[len(prefix):]
    return column

remove_prefix = functools.partial(remove_prefix_in, ['classic_chat_', 'pure_teaching_', 'teaching_chat_'])

for column in sessions.columns:
    session_phases_columns.add(remove_prefix(column))

xp5_sessions = pandas.DataFrame(columns=list(session_phases_columns))

for (_, session) in sessions.iterrows():

    classic_chat_entry = dict()
    classic_chat_entry['session'] = session['session']
    classic_chat_entry['Phase'] = 'classic_chat'

    pure_teaching_entry = dict()
    pure_teaching_entry['session'] = session['session']
    pure_teaching_entry['Phase'] = 'pure_teaching'

    teaching_chat_entry = dict()
    teaching_chat_entry['session'] = session['session']
    teaching_chat_entry['Phase'] = 'teaching_chat'

    for column in sessions.columns:
        if column.startswith('classic_chat_'):
            classic_chat_entry[remove_prefix(column)] = session[column]
        elif column.startswith('pure_teaching_'):
            pure_teaching_entry[remove_prefix(column)] = session[column]
        elif column.startswith('teaching_chat_'):
            teaching_chat_entry[remove_prefix(column)] = session[column]
        else:
            assert(column in ('session', 'experimentDone'))

    xp5_sessions = xp5_sessions.append(classic_chat_entry, ignore_index=True)
    xp5_sessions = xp5_sessions.append(pure_teaching_entry, ignore_index=True)
    xp5_sessions = xp5_sessions.append(teaching_chat_entry, ignore_index=True)


# Now we do the same with the data from previous experiments.
# Experiment ID can be used as a "phase".
# The session ID can be used as a "session".
# The theme is unused.
previous_xp_dir = os.path.join(dir_path, 'previous_xp')
sessions_info = dict({
    'transcription_b1p4_annotated.tsv': ('xp3', 'b1p4', 'Business'),
    'transcription_b2p8_annotated.tsv': ('xp3', 'b2p8', 'Business'),
    'transcription_b3p9_annotated.tsv': ('xp3', 'b3p9', 'Business'),
    'transcription_c1p1_annotated.tsv': ('xp3', 'c1p1', 'Care'),
    'transcription_c2p5_annotated.tsv': ('xp3', 'c2p5', 'Care'),
    'transcription_c3p7_annotated.tsv': ('xp3', 'c3p7', 'Care'),
    'transcription_h1p2_annotated.tsv': ('xp3', 'h1p2', 'Home'),
    'transcription_h2p3_annotated.tsv': ('xp3', 'h2p3', 'Home'),
    'transcription_h3p6_annotated.tsv': ('xp3', 'h3p6', 'Home'),
    'transcription_b1_annotated.tsv': ('xp2', 'b1', 'Business'),
    'transcription_b2_annotated.tsv': ('xp2', 'b2', 'Business'),
    'transcription_b3_annotated.tsv': ('xp2', 'b3', 'Business'),
    'transcription_c1_annotated.tsv': ('xp2', 'c1', 'Care'),
    'transcription_c2_annotated.tsv': ('xp2', 'c2', 'Care'),
    'transcription_c3_annotated.tsv': ('xp2', 'c3', 'Care'),
    'transcription_h1_annotated.tsv': ('xp2', 'h1', 'Home'),
    'transcription_h2_annotated.tsv': ('xp2', 'h2', 'Home'),
    'transcription_h3_annotated.tsv': ('xp2', 'h3', 'Home'),
})

previous_dialogues = None
for file_name, session_info in sessions_info.items():
    file_path = os.path.join(previous_xp_dir, file_name)
    session_dialogues = pandas.read_csv(file_path, sep='\t')
    session_dialogues['XP'], session_dialogues['Session'], session_dialogues['Theme'] = session_info

    # Adding an info telling any ongoing teaching has been abandoned.
    session_dialogues.BA = math.nan
    if math.isnan(session_dialogues.tail(1)['BS']):
        session_dialogues.at[session_dialogues.index[-1], 'BA'] = 1

    if previous_dialogues is None:
        previous_dialogues = session_dialogues
    else:
        previous_dialogues = previous_dialogues.append(session_dialogues, sort=True, ignore_index=True)


# Making the data converge.
# Some columns are renamed, some are ignored.
# Time representation is corrected.
# The more complex change consists in expressing
# when behavior teachings are started, aborted or completed.
behaviors = behaviors.rename(
    columns={'session': 'Session', 'phase': 'Phase'})
behaviors['XP'] = 'xp5'

dialogues['Theme'] = 'Home'
dialogues['XP'] = 'xp5'
dialogues = dialogues.rename(
    columns={'session': 'Session', 'phase': 'Phase', 'time': 'Time',
            'result': 'Result', 'speaker': 'Speaker',
            'transcription': 'Transcription'})

dialogues['Teaching Started'] = dialogues.bs.transform(
    lambda bs: 1 if bs in ('B', 'S') else math.nan
)
dialogues['Teaching Completed'] = dialogues.bs.transform(
    lambda bs: 1 if bs == 'G' else math.nan
)
dialogues['Teaching Aborted'] = dialogues.bs.transform(
    lambda bs: 1 if bs in ('A', 'S') else math.nan
)

dialogues = dialogues.drop(columns=['comment', 'bs'])


xp5_sessions = xp5_sessions.rename(
    columns={'session': 'Session', 'ets': 'ETS', 'eie': 'EIE'}
)
xp5_sessions['XP'] = 'xp5'


previous_dialogues['Phase'] = 'pure_teaching'
previous_dialogues['Time'] = previous_dialogues['Time'].transform(
    lambda time: time.replace(',', '.')
)
previous_dialogues['Teaching Started'] = previous_dialogues['NB'].transform(
    lambda nb: 1 if not math.isnan(nb) else math.nan
)
previous_dialogues['Teaching Completed'] = previous_dialogues['BS'].transform(
    lambda bs: 1 if not math.isnan(bs) else math.nan
)

teaching_ongoing = False
def detect_teaching_abortion(row):
    global teaching_ongoing
    if not math.isnan(row.BA):
        assert math.isnan(row.BS)
        assert math.isnan(row.NB)
        if teaching_ongoing:
            teaching_ongoing = False
            return 1
        else:
            return math.nan
    elif not math.isnan(row.BS):
        assert teaching_ongoing
        assert math.isnan(row.NB)
        teaching_ongoing = False
        return math.nan
    elif not math.isnan(row.NB):
        if teaching_ongoing:
            return 1
        else:
            teaching_ongoing = True
            return math.nan
    return math.nan

previous_dialogues['Teaching Aborted'] = previous_dialogues.aggregate(
    lambda row: detect_teaching_abortion(row),
    axis=1
)
previous_dialogues = previous_dialogues.drop(columns=['NB', 'BS', 'BA'])

previous_dialogues['Speaker'] = previous_dialogues['Speaker'].transform(
    lambda speaker: 'H' if speaker == 'User' else 'R'
)
previous_dialogues = previous_dialogues.rename(
    columns={'Said': 'Transcription', 'Mis': 'Result'}
)

dialogues = dialogues.append(
    previous_dialogues, sort=True, ignore_index=True)

instructions = dialogues[(dialogues.Speaker == 'H')]
# This below raises a false positive warning.
instructions.Result.fillna(value='G', inplace=True)

# Merging the data together.
# The index to identify a session + phase is "all_index".
# The total frame with all sessions can be built from the dialogues,
# Here by counting the number of user instructions.
all_index = ['XP', 'Phase', 'Session']
all_sessions = pandas.DataFrame(instructions.groupby(all_index).count().Speaker)
all_sessions = all_sessions.rename(columns={'Speaker': 'IC'})
xp5_sessions.set_index(all_index, inplace=True)
all_sessions.loc['xp5', 'EIE'] = xp5_sessions['EIE']
all_sessions.loc['xp5', 'ETS'] = xp5_sessions['ETS']

# Writing qualitative data out in a separate file.
qualitative_data = xp5_sessions[['other_comments', 'what_to_improve', 'missing_behaviors', 'refrained_behaviors']]
qualitative_data_path = os.path.join(dir_path, 'qualitative_data.tsv')
qualitative_data.to_csv(qualitative_data_path, sep='\t')

# Then we add all measurements that are to be counted.
# Then we compute averages.

# Counting the number of instructions per phase + session
xp5_sessions['IC'] = instructions.groupby(all_index).count().Speaker

# Number of instructions per phase
phases_index = ['XP', 'Phase']
all_phases = all_sessions[['IC']].sum(level=phases_index)

# Number of sessions per phase
all_phases['Sessions'] = all_sessions.groupby(phases_index).count()['IC']

# Counting Err
instructions['Err'] = instructions['Result'] == 'A'
instructions_err = instructions[instructions.Result == 'A']
all_sessions['Err'] = instructions_err.groupby(all_index).count()['Result']

# Counting Mis
instructions['Mis'] = instructions['Result'].transform(lambda res: res in ('S', 'D', 'U'))
instructions_mis = instructions[instructions.Result.transform(lambda res: res in ('S', 'D', 'U'))]
all_sessions['Mis'] = instructions_mis.groupby(all_index).count()['Result']

# Counting Teaching Starts, Abortions and Completions
teaching_cols = ['Teaching Started', 'Teaching Completed', 'Teaching Aborted']
teaching_info = dialogues[all_index + teaching_cols]
all_sessions[teaching_cols] = teaching_info.groupby(all_index).sum()
assert((all_sessions['Teaching Started'] == all_sessions['Teaching Aborted'] + all_sessions['Teaching Completed']).all())

# We can sum most of this data per phase.
all_phases = all_sessions.groupby(phases_index).sum()

# And here are the totals.
xp5_total = all_sessions.loc['xp5'].sum()
all_total = all_sessions.sum()

# Counting users that had a successful teaching.
# UTA is the number of users that attempted a teaching.
# UTS is the number of users that completed a teaching.
# There is one user per session, and several phases per user / session.
all_sessions['UTA'] = all_sessions['Teaching Started'] > 0
all_sessions['UTS'] = all_sessions['Teaching Completed'] > 0

# We count the number of sessions that has UTA or UTS = True using sum()
# Note that UTA and UTS does not simply sum up to the session level,
# because it relates to the users.
# So if a user manages to teach in one phase, but not in another one,
# then it is considered a success.
all_phases[['UTA', 'UTS']] = all_sessions[['UTA', 'UTS']].sum(level=phases_index)
xp5_uts = all_sessions[['UTA', 'UTS']].groupby(['XP', 'Session']).any().loc['xp5'].sum()
xp5_total.loc['UTA'] = xp5_uts['UTA']
xp5_total.loc['UTS'] = xp5_uts['UTS']

# Rate of user succeeding the teaching: UTS%
all_phases['UTS%'] = 100.0 * all_phases['UTS'] / all_phases['UTA']
xp5_total.loc['UTS%'] = 100.0 * xp5_total.loc['UTS'] / xp5_total.loc['UTA']

# UTSC: looking at the taught behaviors,
# we try to find if there is a recipe containing a taught label,
# per phase.
previous_index = None
known_labels = dict() # {(session, phase): labels = set()}
all_sessions = all_sessions.assign(UTSC=False)

for (_, behavior) in behaviors.iterrows():
    index = ('xp5', behavior['Phase'], behavior['Session'])
    if index not in known_labels:
        known_labels[index] = list()
    known_labels[index].append(behavior['label'])

for (_, behavior) in behaviors.iterrows():
    index = ('xp5', behavior['Phase'], behavior['Session'])
    for label in known_labels[index]:
        if label in behavior['recipe']:
            all_sessions.loc[index, 'UTSC'] = True

all_phases['UTSC'] = all_sessions[['UTSC']].groupby(phases_index).sum()

# Same for UTSC%
xp5_total.loc['UTSC'] = all_sessions[['UTSC']].groupby(['Session']).any()['UTSC'].sum()
all_phases['UTSC%'] = 100.0 * all_phases['UTSC'] / all_phases['UTA']
xp5_total.loc['UTSC%'] = 100.0 * xp5_total.loc['UTSC'] / xp5_total.loc['UTA']

# All below are averages.
# The first ones are trivial. Then it gets harder.
all_sessions['Err%'] = 100.0 * all_sessions['Err'] / all_sessions['IC']
all_phases['Err%'] = 100.0 * all_phases['Err'] / all_phases['IC']
xp5_total.loc['Err%'] = 100.0 * xp5_total.loc['Err'] / xp5_total.loc['IC']
all_total.loc['Err%'] = 100.0 * all_total.loc['Err'] / all_total.loc['IC']

all_sessions['Mis%'] = 100.0 * all_sessions['Mis'] / all_sessions['IC']
all_phases['Mis%'] = 100.0 * all_phases['Mis'] / all_phases['IC']
xp5_total.loc['Mis%'] = 100.0 * xp5_total.loc['Mis'] / xp5_total.loc['IC']
all_total.loc['Mis%'] = 100.0 * all_total.loc['Mis'] / all_total.loc['IC']

all_sessions['TS%'] = 100.0 * all_sessions['Teaching Completed'] / all_sessions['Teaching Started']
all_phases['TS%'] = 100.0 * all_phases['Teaching Completed'] / all_phases['Teaching Started']
xp5_total.loc['TS%'] = 100.0 * xp5_total.loc['Teaching Completed'] / xp5_total.loc['Teaching Started']
all_total.loc['TS%'] = 100.0 * all_total.loc['Teaching Completed'] / all_total.loc['Teaching Started']

all_sessions['LRt'] = behaviors[all_index + ['LRt']].groupby(all_index).mean()
all_phases['LRt'] = behaviors[phases_index + ['LRt']].groupby(phases_index).mean()
all_total['LRt'] = behaviors['LRt'].mean()

# Double-check consistency with annotations.
assert(xp5_total['Teaching Completed'] == nof_behaviors_annotated)

# Computing the time for teaching, At w Err:
# from start to G, and from G to next G.
# Here 'G' has become a bool on the column 'Teaching Completed'
# Time to Teach measure: like At w Err, but knowing the start.
# We check for B or S, as a sign of the beginning of a teaching.
# If we find A, we drop it.
# If we find G, we register it.
previous_session_index = None
start_time_at_w_err = None
start_time_tts = None
dialogues['At w Err'] = math.nan
dialogues['TtS'] = math.nan
for (index, dialogue) in dialogues.iterrows():
    # Note that the data must be ordered by session / phase
    session_index = (dialogue['XP'], dialogue['Phase'], dialogue['Session'])
    new_time = float(dialogue['Time'])

    # If we switch session, replace the previous time
    # with the start time of that session.
    if session_index != previous_session_index:
        start_time_at_w_err = None
        start_time_tts = None

    previous_session_index = session_index

    if not math.isnan(dialogue['Teaching Aborted']) and dialogue['Teaching Aborted']:
        start_time_tts = None

    if not math.isnan(dialogue['Teaching Started']) and dialogue['Teaching Started']:
        if start_time_at_w_err is None:
            start_time_at_w_err = new_time
        start_time_tts = new_time

    # Besides this, we're only interested in teaching completions.
    if not math.isnan(dialogue['Teaching Completed']) and dialogue['Teaching Completed'] == True:

        # We reached a teaching completion.
        # This never happens in the classic chat.
        # This never happens before a behavior is started.
        assert dialogue['Phase'] != 'classic_chat'
        assert start_time_at_w_err is not None
        assert start_time_tts is not None

        new_time = float(dialogue['Time'])
        dialogues.loc[index, 'At w Err'] = new_time - start_time_at_w_err
        dialogues.loc[index, 'TtS'] = new_time - start_time_tts
        start_time_at_w_err = new_time
        start_time_tts = None

behavior_times_cols = ['At w Err', 'TtS']
behavior_times_index = all_index + behavior_times_cols
behavior_times = dialogues[behavior_times_index]

behavior_times_session = behavior_times.groupby(all_index).mean()
all_sessions[behavior_times_cols] = behavior_times_session

behavior_times_by_phase = behavior_times.groupby(phases_index).mean()
all_phases[behavior_times_cols] = behavior_times_by_phase

behavior_times_total = behavior_times.mean()
all_total = all_total.append(behavior_times_total)

behavior_times_by_xp = behavior_times.groupby(['XP']).mean()
xp5_total = xp5_total.append(behavior_times_by_xp.loc['xp5'])

# ETS / EIE
# KLUDGE: .mean() does not work on groups that only contains NaN.
def very_mean(group):
    try:
        return group.mean()
    except pandas.DataError:
        return pandas.DataFrame(math.NaN).reindex_like(pandas.DataFrame(group))

phases_ets_eie = all_sessions[['ETS', 'EIE']].groupby(phases_index).apply(very_mean)
xp5_ets_eie = all_sessions.loc['xp5', ['ETS', 'EIE']].mean()
total_ets_eie = all_sessions[['ETS', 'EIE']].mean()
all_phases['ETS'] = phases_ets_eie['ETS']
all_phases['EIE'] = phases_ets_eie['EIE']
xp5_total['ETS'] = xp5_ets_eie['ETS']
xp5_total['EIE'] = xp5_ets_eie['EIE']
all_total['ETS'] = total_ets_eie['ETS']
all_total['EIE'] = total_ets_eie['EIE']

# SUS
# From Brooke, J. (1996). SUS-A quick and dirty usability scale.
# Usability evaluation in industry, 189(194), 4–7:
# "To calculate the SUS score,
# first sum the score contributions from each item.
# Each item's score contribution will range from 0 to 4.
# For items 1,3,5,7,and 9 the score contribution is
# the scale position minus 1. For items 2,4,6,8 and 10,
# the contribution is 5 minus the scale position.
# Multiply the sum of the scores by 2.5 to obtain
# the overall value of SU."
xp5_sessions = xp5_sessions.assign(
    SU=lambda row: (row.sus_frequency - 1) + (5 - row.sus_complexity) +
        (row.sus_easy_use - 1) + (5 - row.sus_control) +
        (row.sus_integration - 1) + (5 - row.sus_incoherence) +
        (row.sus_quick_learn - 1) + (5 - row.sus_heavy) +
        (row.sus_confidence - 1) + (5 - row.sus_learn_amount))

all_sessions.loc['xp5', 'SU'] = xp5_sessions['SU']
all_phases.loc['xp5', 'SU'] = xp5_sessions[['SU']].groupby(phases_index).apply(very_mean)['SU']
xp5_total.loc['SU'] = xp5_sessions['SU'].mean()

# Users feels.
xp5_feels = xp5_sessions.loc[('xp5', 'teaching_chat'), ['more_connected', 'more_worth_interacting', 'robot_is_entertaining', 'robot_is_more_entertaining', 'robot_is_useful', 'robot_is_more_useful']]
xp5_total = xp5_total.append(xp5_feels.mean())
xp5_total_std = xp5_feels.std()

# Put together results for presentation.
summary = all_phases.transpose()
summary[('xp5', 'Total')] = xp5_total
summary['Total'] = all_total
summary_path = os.path.join(dir_path, 'summary.tsv')
summary.to_csv(summary_path, sep='\t')
print('Summary:\n%s' % summary)
print('-----------------------------------------------------')

print('User Appreciation Summary:\n%s' % xp5_total)
print('Standard Deviations:\n%s' % xp5_total_std)
print('-----------------------------------------------------')

# Now, deeper statistical analysis.
def test_independence_contingency(contingency_table):
    if (contingency_table < 5).sum(1).sum(0) > 0:
        print('There is fewer than 5 occurrences of a sample, test may be invalid')
    # >30 is ideal
    chi2, p, dof, expected = scipy.stats.chi2_contingency(contingency_table)
    print('chi2 = %s\np = %s' % (chi2, p))

    if p < 0.05:
        print('Variables are not independent')
    elif p > 0.95:
        print('Variables are independent')
    else:
        print('Inconclusive result')


def test_independence_ttest(series_a, series_b):
    t, p = scipy.stats.ttest_ind(series_a, series_b)
    print('t = %s\np = %s' % (t, p))

    if p < 0.05:
        print('Variables are not independent')
    elif p > 0.95:
        print('Variables are independent')
    else:
        print('Inconclusive result')


# Testing ASR error between classic_chat and teaching_chat
#-----------------------------------------------------------
instructions_chat = instructions[instructions['Phase'] != 'pure_teaching']
chat_err_count = pandas.crosstab(
    instructions_chat.Phase,
    instructions_chat.Result == 'A',
    margins=True)
print('Phase vs. ASR Error, contingency table:\n%s'
        % chat_err_count)
test_independence_contingency(chat_err_count)
print('-----------------------------------------------------')


# Testing NLU error between classic_chat and teaching_chat
#-----------------------------------------------------------
chat_mis_count = pandas.crosstab(
    instructions_chat.Phase,
    instructions.Result.transform(lambda res: res in ('S', 'D', 'U')),
    margins=True)
print('Phase vs. NLU Error, contingency table:\n%s'
        % chat_mis_count)
test_independence_contingency(chat_mis_count)
print('-----------------------------------------------------')


# Testing SU between classic_chat and teaching_chat
#-----------------------------------------------------------
# Note that we make sure that we take only the sessions
# that for which we have the data for both phases.
classic_chat_su = all_sessions.loc[('xp5', 'classic_chat'), 'SU'].dropna()
teaching_chat_su = all_sessions.loc[('xp5', 'teaching_chat'), 'SU'].dropna()
chat_su_common_sessions = set(classic_chat_su.index) & set(teaching_chat_su.index)
classic_chat_su = classic_chat_su.loc[chat_su_common_sessions]
teaching_chat_su = teaching_chat_su.loc[chat_su_common_sessions]
print('Phase vs. SU:\nClassic Chat:\n%s\nTeaching Chat:\n%s' %
        (classic_chat_su, teaching_chat_su))
test_independence_ttest(classic_chat_su, teaching_chat_su)
print('-----------------------------------------------------')


# Testing whether, for chat-related phases,
# Err% is correlated with SU.
#-----------------------------------------------------------
classic_chat_su_err = all_sessions.loc[('xp5', 'classic_chat'), ['SU', 'Err%']].dropna()
teaching_chat_su_err = all_sessions.loc[('xp5', 'teaching_chat'), ['SU', 'Err%']].dropna()
su_err = classic_chat_su_err.append(teaching_chat_su_err, ignore_index=True)
su_err['SU'] = su_err['SU'].transform(lambda x: float(x))
print('SU vs. Err in chat-enabled phases:\n%s' % su_err)
print('Pearson Correlation:')
print(scipy.stats.pearsonr(su_err['SU'], su_err['Err%']))
print('Spearman Correlation:')
print(scipy.stats.spearmanr(su_err['SU'], su_err['Err%']))
print('-----------------------------------------------------')


# Testing whether, across all phases,
# Err% is correlated with SU.
#-----------------------------------------------------------
su_err = all_sessions.loc['xp5', ['SU', 'Err%']].dropna().reset_index(drop=True)
su_err['SU'] = su_err['SU'].transform(lambda x: float(x))
print('SU vs. Err in all phases of XP 5:\n%s' % su_err)
print('Pearson Correlation:')
print(scipy.stats.pearsonr(su_err['SU'], su_err['Err%']))
print('Spearman Correlation:')
print(scipy.stats.spearmanr(su_err['SU'], su_err['Err%']))
print('-----------------------------------------------------')


# Testing whether teaching success is correlated to phases.
#-----------------------------------------------------------
pure_teaching_ts = all_sessions.loc[('xp5', 'pure_teaching'), 'TS%'].dropna()
teaching_chat_ts = all_sessions.loc[('xp5', 'teaching_chat'), 'TS%'].dropna()
print('Phase vs. TS:\nPure Teaching:\n%s\nTeaching Chat:\n%s' %
        (pure_teaching_ts, teaching_chat_ts))
test_independence_ttest(pure_teaching_ts, teaching_chat_ts)
print('-----------------------------------------------------')

# Same test, on teaching attempts instead of on teaching success rate.
# As a consequence it becomes biased by the proficiency of certain users.
xp5_teaching_dialogues = dialogues[dialogues['Phase'].transform(lambda phase: phase in ('pure_teaching', 'teaching_chat'))]
xp5_teaching_dialogues = xp5_teaching_dialogues[xp5_teaching_dialogues['XP'] == 'xp5']
xp5_teaching_dialogues = xp5_teaching_dialogues[['Session', 'Phase', 'Teaching Aborted', 'Teaching Completed']]
xp5_teaching_dialogues = xp5_teaching_dialogues[xp5_teaching_dialogues['Teaching Aborted'].notna() | xp5_teaching_dialogues['Teaching Completed'].notna()]

xp5_teaching_ts_count = pandas.crosstab(
    xp5_teaching_dialogues.Phase,
    xp5_teaching_dialogues['Teaching Completed'].notna(),
    margins=True)
print('Phase vs. Teaching Completed, contingency table:\n%s'
        % xp5_teaching_ts_count)
test_independence_contingency(xp5_teaching_ts_count)
print('-----------------------------------------------------')


# Testing whether teaching success is correlated experiments.
# First let's try XP 3 vs. XP 5, pure_teaching.
#-----------------------------------------------------------
xp3_ts = all_sessions.loc[('xp3', 'pure_teaching'), 'TS%'].dropna()
print('XP / Phase vs. TS:\nXP 3:\n%s\nXP 5, Pure Teaching:\n%s' %
        (xp3_ts, pure_teaching_ts))
test_independence_ttest(xp3_ts, pure_teaching_ts)
print('-----------------------------------------------------')

# Same test, on teaching attempts instead of on teaching success rate.
# As a consequence it becomes biased by the proficiency of certain users.
pure_dialogues = dialogues[dialogues['Phase'] == 'pure_teaching']
xp35_dialogues = pure_dialogues[pure_dialogues['XP'].transform(lambda xp: xp in ('xp3', 'xp5'))]
xp35_teaching_dialogues = xp35_dialogues[['XP', 'Teaching Aborted', 'Teaching Completed']]

xp35_teaching_dialogues = xp35_teaching_dialogues[xp35_teaching_dialogues['Teaching Aborted'].notna() | xp35_teaching_dialogues['Teaching Completed'].notna()]

xp35_ts_count = pandas.crosstab(
    xp35_teaching_dialogues.XP,
    xp35_teaching_dialogues['Teaching Completed'].isna(),
    margins=True)
print('XP vs. Teaching Completed, contingency table:\n%s'
        % xp35_ts_count)
test_independence_contingency(xp35_ts_count)
print('-----------------------------------------------------')

# Testing whether the rate of users succeeding is correlated to phases.
#-----------------------------------------------------------
phase_uts = all_sessions.loc[
    ('xp5', 'pure_teaching'):('xp5', 'teaching_chat'), 'UTS'].reset_index()
phase_uts_count = pandas.crosstab(
    phase_uts.Phase,
    phase_uts.UTS,
    margins=True)
print('Phase vs. UTS, contingency table:\n%s'
        % phase_uts_count)
test_independence_contingency(phase_uts_count)
print('-----------------------------------------------------')


# Testing whether teaching success is correlated to err.
#-----------------------------------------------------------
ts_err = all_sessions[['TS%', 'Err%']].dropna().reset_index()
print('TS vs. Err (XP 3 and 5, teaching phases):\n%s' % ts_err)
print('Pearson Correlation:')
print(scipy.stats.pearsonr(ts_err['TS%'], ts_err['Err%']))
print('Spearman Correlation:')
print(scipy.stats.spearmanr(ts_err['TS%'], ts_err['Err%']))
print('-----------------------------------------------------')


# Testing whether teaching success is correlated to err,
# this time only with phases that are "pure teaching".
#-----------------------------------------------------------
ts_err = ts_err[ts_err['Phase'] == 'pure_teaching']
print('TS vs. Err (XP 3 and 5, pure teaching only):\n%s' % ts_err)
print('Pearson Correlation:')
print(scipy.stats.pearsonr(ts_err['TS%'], ts_err['Err%']))
print('Spearman Correlation:')
print(scipy.stats.spearmanr(ts_err['TS%'], ts_err['Err%']))
print('-----------------------------------------------------')


# Testing whether teaching success is correlated to err,
# this time only with phases that are "pure teaching",
# and removing the experiments were it did not work at all.
#-----------------------------------------------------------
ts_err = ts_err[ts_err['TS%'] != 0]
print('TS vs. Err (XP 3 and 5, pure teaching, 0s):\n%s' % ts_err)
print('Pearson Correlation:')
print(scipy.stats.pearsonr(ts_err['TS%'], ts_err['Err%']))
print('Spearman Correlation:')
print(scipy.stats.spearmanr(ts_err['TS%'], ts_err['Err%']))
print('-----------------------------------------------------')


# Testing whether Err significantly grows between xp3 and xp5
#-----------------------------------------------------------
pure_instructions = instructions[instructions['Phase'] == 'pure_teaching']
xp35_instructions = pure_instructions[pure_instructions['XP'].transform(lambda xp: xp in ('xp3', 'xp5'))]
xp35_err_count = pandas.crosstab(
    xp35_instructions.XP,
    xp35_instructions.Err,
    margins=True)
print('XP vs. Err, contingency table:\n%s'
        % xp35_err_count)
test_independence_contingency(xp35_err_count)
print('-----------------------------------------------------')


# Testing whether Mis significantly grows between xp3 and xp5
#-----------------------------------------------------------
xp35_mis_count = pandas.crosstab(
    xp35_instructions.XP,
    xp35_instructions.Mis,
    margins=True)
print('XP vs. Mis, contingency table:\n%s'
        % xp35_mis_count)
test_independence_contingency(xp35_mis_count)
print('-----------------------------------------------------')

# For users skill: we can check whether the good users
# in one phase are still good in the other phase:
# spearman of TS% between the two phases.
#-----------------------------------------------------------
xp5_teaching_dialogues_by_session = xp5_teaching_dialogues.groupby('Session').sum()
xp5_teaching_dialogues_by_session['Teaching Attempted'] = xp5_teaching_dialogues_by_session['Teaching Aborted'] + xp5_teaching_dialogues_by_session['Teaching Completed']
xp5_teaching_dialogues_by_session['TS%'] = 100.0 * xp5_teaching_dialogues_by_session['Teaching Completed'] / xp5_teaching_dialogues_by_session['Teaching Attempted']
xp5_teaching_dialogues_by_session.sort_values(by='TS%', inplace=True)
print('TS by Session:\n%s' % xp5_teaching_dialogues_by_session[['TS%', 'Teaching Attempted']])
print('Pearson Correlation:')
print(scipy.stats.pearsonr(xp5_teaching_dialogues_by_session['TS%'], xp5_teaching_dialogues_by_session['Teaching Attempted']))
print('Spearman Correlation:')
print(scipy.stats.spearmanr(xp5_teaching_dialogues_by_session['TS%'], xp5_teaching_dialogues_by_session['Teaching Attempted']))
print('-----------------------------------------------------')

# Per phase, number of taught behaviors per person, mean and deviation.
# We focus on sessions where there was a teaching attempt.
teaching_sessions = all_sessions[all_sessions['UTA']]
teaching_phases = teaching_sessions[['Teaching Completed']].groupby(phases_index).sum()
teaching_phases['Teaching Completed Mean'] = teaching_sessions['Teaching Completed'].groupby(phases_index).mean()
teaching_phases['Teaching Completed STD'] = teaching_sessions[['Teaching Completed']].groupby(phases_index).std()
teaching_phases['Sessions'] = teaching_sessions[['Teaching Completed']].groupby(phases_index).count()
teaching_phases['UTS'] = all_phases['UTS']
teaching_phases['UTSC'] = all_phases['UTSC']
teaching_phases['LRt'] = all_phases['LRt']
print('Mean number of taught behaviors:')
print(teaching_phases)