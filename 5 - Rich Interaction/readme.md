# Experiment #5

## Background

This experiment is the final one of
Victor Paléologue's PhD thesis.
Thanks to the help of Jocelyn Martin,
Shin Watanabe and Jérôme Bruzaud,
several teaching behaviors
have been put together.
This experiment will show how
to take advantage of these behaviors,
while avoiding conflict between each other.

The experiment takes place in
the same context as previous experiment:
Pepper@Home.
Pepper@Home is a program lending robots
to SoftBank Robotics Europe employees,
in exchange for the participation
so some experiments,
or for the testing of some applications.

Participants have contractually accepted
to be recorded in order to improve
the software they end up testing.

## Objectives

Test a new behavior model accepting parameters,
and its associated teaching interaction,
to look for an improvement of
the overall teaching interaction,
the ease of teaching.
The same measures as in previous
experiment will be used
to ensure they are comparable.
For the first time we would like also
to measure a sense of utility,
and a sense of entertainment.

The context is still an open scenario,
and therefore there is some potential
preparation needed, so that the robot
has gathered some information about the world:
places, things, people.
We would let the robot learn some things
about the world first.
The teaching interactions will be evaluated too,
to check that they are not completely failing.

Because of the new decision-making system,
and this set of teaching behaviors,
some conflicts may appear.
We must know whether the rules were effectively applied,
whether their application solved the conflicts,
and whether new, unexpected, conflicts took place.

## Setup

Note that all Pepper@Home robots
are Pepper 1.8a robots,
that were already upgraded to
2.9.x versions.

### Android Application

We updated DEF,
the Android application used in experiment #4,
so that it applies the protocol
defined further below in this document.
The "Teaching Experiment" activity
refuses to start before
all requirements are met.

TODO: attach the .apk

### NAOqi

To align with NAOqi development requirements,
the system was updated to the 2.9.3.114.
Our NAOqi packages were compiled agains
this version's toolchain.

We deployed the semantic package
version xxx
TODO: attach the .pkg,
It includes the SemanticAgent service
implementing our system,
and the RecordUploder service,
that sends the recordings online.

The recordings are produced
by the MultimodalRecorder service
provided by the same
multimodal recorder package
that was deployed experiment #4.
TODO: attach the .pkg

We deployed also the same cognitive services.
TODO: attach the .pkg

We added a new package
providing a NAOqi interface
to fast-downward's PDDL planner,
actionplanner-xxx.pkg
TODO: attach and link

Finally, we added the adaptation
of Bahar Irfan's work to NAOqi 2.9
by Jérôme Bruzaud,
in the package
multimodalrecognition-xxx.pkg
TODO: attach and link

### Online Collection

The same server used in experiment #4
is used for this experiment.

### Installation instructions

TODO: provide e-mail content

## Protocol

Participants are informed of the update of DEF
and of NAOqi applications.
Once the update is complete,
they are asked to run the main activity of DEF.

Participants were told that the robot
would try to explore the world
(and required their charging flap to be closed),
and that it may to learn their identity.
They must let their robot perform some exploration,
get it to ask for the name of a point of interest,
and get it to know their name.

Once these conditions are met for the first time,
the robot proposes to do the experiment #5,
if it was never done on that robot.
Accepting switches the robot
to the "Teaching Experiment" activity.

The teaching experiment occurs
in that activity, once:

- the inititial teaching of point of interest
  and of people was performed.
- the participants have accepted to be recorded
  during the rest of the experiment.

### 1. Knowledge Initialization

Initially, the robot knows nothing
of the world, and may not be able
to take advantage of its parametrable
behaviors.

Also, the participants are not aware
of the learning capabilities of the robot,
and should perform some teachings
at least once,
so that we can ask them about
these behaviors.

We oblige the participants
to perform at least one teaching of each,
before they can leave this phase
and switch to the rest of the experiment.

### 2. Separate Behaviors

The two first phases are supposed to be independent, and should be tested in a random order.

### a) Classic Chat

In this phase we evaluate the baseline interaction with the robot in Pepper@Home. No recording is required.

The robot is configured to use the baseline content on a standard chat, in the current language of the robot.

They are asked to exchange with the robot at least 30 times. When done, the interaction stops and they are directed to a questionnaire, that includes an evaluation on the [System Usability Scale (SUS)](https://www.usability.gov/how-to-and-tools/methods/system-usability-scale.html), and extra optional input:

 * What issues did you encounter during this phase? What could be improved?

 * Other comments:

Questions order is randomized. When done, the app switches to the next phase.

### 2.b) Teaching behaviors only

In this phase,
we evaluate the teaching algorithms,
in a comparable way to how it was done
in the previous experiments.
This phase requires at least the audio to be recorded. If the participant disagrees,
the experiment is interrupted.

The second phase consists in showing equivalent instructions as the sheets shown in previous studies.
A vocal explanation is given by the robot.
A button lets the user request the instructions again. When they start speaking to the robot,
a 10 minutes countdown starts.
They have to compose as many behaviors as possible.

![Instructions for teaching behaviors](screen pure teaching instructions.png)

After the 10 minutes are passed, the screen switches to the questionnaire, with the classic chat's questionnaire, plus the same questions as in previous experiments:

- whether they thought that the teaching was successful, on the Likert scale.
- whether the overall interaction was easy, on the Likert scale.

Questions order is randomized.

From the collected data, we will compute:

- The number of utterances (or instructions) provided by the users (IC).

- The average rate of instructions misrecognized (Mis%).

- The average time in seconds needed to teach a behavior, including the time lost with misrecognized instructions or any other error (AT w Err).

- The percentage of behaviors that were successfully taught, among the total number of behaviors attempted to be taught (TS%).

- The percentage of users who managed to teach behaviors (UTS%).

- The percentage of users who managed to teach composite behaviors (UTSC%).

- How successful the teaching felt. It is the Experienced Teaching Success (ETS).

- How easy the interaction felt. It is the Experienced Interaction Ease (EIE).

### 3. Interaction augmented with teaching

In that third phase,
participants are recorded in video.
The app is configured to mix
the baseline interaction with the teaching behaviors,
using the SemanticAgent's chat.

The robot and the app tells
to the participant that
he or she is required
to teach two behaviors to the robot,
while the robot supports the baseline interaction.
When the participant has taught the two behaviors,
the last questionnaire appear,
mixing questionnaires from
phases 2.a) and 2.b).

### 4. In the wild

The controlled experiment is now over,
but the application remains available.
When run again, the first time,
it is asked whether to enable the teaching or not.
It is also told how to change this later on
(through a setting).

If the teaching is activated,
the app behaves like in phase 3,
but with no instructions,
and no questionnaire at the end.

Online, according to Pepper@Home's cycles,
we will be able to ask the following questions:

TODO: update with the real questions asked.
- Do you feel more connected
  to your robot now?
- Do you think it is more worth interacting
  with the robot now?
- What behavior did you refrain to teach?
- What behavior would you have liked
  the robot to know already?
- Any other comment on the teaching of behaviors?
- Do you think the overall behavior
  of the robot is entertaining?
- Do you think the overall behavior
  is more entertaining than before?
- Do you think the overall behavior
  of the robot is useful?
- Do you think the overall behavior
  is more useful than before?

Questions order is randomized.

## Data Analysis

Session recordings were collected by SBRE.
It contains personal data and is not publicly available.
Non-personal data, including the dialogues,
the taught behaviors, and the questionnaire replies,
can be found unders `text_data`, in text files.
These are a serialization of the data collected
as Android preferences.
Run `data_extraction.py` to perform the extraction
of the data into tabular data:
- `extracted_sessions.tsv` contains all the data per session;
- `extracted_dialogues.tsv` contains all dialogues per phase;
- `extracted_behaviors.tsv` contains the taught behaviors per phase.

It appeared that in many sessions,
what was said by our custom behaviors did not end
in the collected data.
It sums up to everything but the classical chat content.
The experimenter reviewed the recordings
to transcribe these missing utterances.
These transcriptions can be found in
`robot_utterance_transcriptions`

We regroup these transcriptions with
the automatically collected data
from `extracted_dialogues.tsv`
and annotate the dialogues
in `extracted_dialogues_annotated.tsv`,
that can be read, as follows:

- `session`: the ID of the experimental session.
  One session corresponds to a run of the teaching
  experiment activity.
- Phase: the ID of the phase within the experimental session,
  among `pure_teaching` or `teaching_chat`.
- `time`: the time computed since the first utterance
  of the user. It may be offset compared
  to the video records' timestamps.
- `speaker`: `H` stands for human user,
  `R` stands for robot.
- `transcription`: the transcription of the user's utterance
  by the experimenter, considered perfect.
- `result`: indicates how the understanding performed,
  including the ASR:
  - `A` stands for ASR errors;
  - `S` stands for semantic extraction error;
  - `U` stands for unsupported by the behaviors;
  - `D` stands for dialog logic issue;
    for instance if the teaching kept on although
    it shouldn't have. It's also a `U`.
  - `F` stands for execution failure;
  - `G` stands as good: it went well.
  Note that there is no transcription entry
  if the ASR did not catch anything.
- `bs`:
  - `B` when the intention to teach a new
    behavior is expressed.
  - `S` when the behavior currently being taught
    is abandoned, and a new one is begun.
  - `A` when the teaching of behavior was abandoned.
  - `G` when the teaching of behavior was successful.
- `comment`: extra column for arbitrary comments.

To understand some of the annotations,
or some of the session data,
we carried on a qualitative analysis on the sessions.
It can be found in `qualitative_analysis.txt`.