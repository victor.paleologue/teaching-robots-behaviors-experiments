This repository gathers together the information about experiments held during
Victor Paléologue's PhD studies on "teaching robots behaviors" (2016-2019).

[Experiment #1](1%20-%20Preliminary) is mentioned the PhD manuscript
(not published).

[Experiment #2](2%20-%20Transcribed) was used for the article
Paléologue, Victor, Jocelyn Martin, Amit Kumar Pandey, Alexandre Coninx, et Mohamed Chetouani.
« Semantic-based interaction for teaching robot behavior compositions ».
In 2017 26th IEEE International Symposium on Robot and Human Interactive Communication (RO-MAN).
Lisbon, Portugal, 2017.
DOI: 10.1109/ROMAN.2017.8172279.

[Experiment #3](3%20-%20Automatic) was used for the article
Paléologue, Victor, Jocelyn Martin, Amit Kumar Pandey, et Mohamed Chetouani.
« Semantic-based interaction for teaching robot behavior compositions using spoken language ».
In International Conference on Social Robotics, 421–430. Springer, 2018.
DOI: 10.1007/978-3-030-05204-1_41.

[Experiment #4](4%20-%20In%20the%20wild)
is based on an experiment run in homes.
The application did not run properly,
preventing the experiment to be completed by participants.
It is therefore mostly incomplete.

[Experiment #5](5%20-%20Rich%20interaction)
is based on an experiment run in homes.
It is used for Victor Paléologue's PhD thesis.

At the root level,
we collected some files written in the early phases of the PhD.
They describe the protocols and the specifications
that we envisioned for the experiments.
